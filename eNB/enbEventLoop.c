#include "enbEventLoop.h"
#include "generateFrame.h"

int socket_handle(int net_flag, int addr_flag, int socket_lvl, int type)
{
  int server_socket_fd = socket(net_flag, type, 0);

  if (server_socket_fd < 0)
  {
    perror("Error opening socket");
    return -1;
  }

  int opt = 1;
  if (setsockopt(server_socket_fd, socket_lvl, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt)) < 0)
  {
    perror("setsockopt");
    return -1;
  }

  struct sockaddr_in server_addr;
  bzero((char*) &server_addr, sizeof(server_addr));
  server_addr.sin_family = net_flag;
  server_addr.sin_addr.s_addr = addr_flag; //bind the socket to all available interfaces
  server_addr.sin_port = htons(PORT);      //converts byte order

  if (bind(server_socket_fd, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0)
  {
    perror("Error bind");
    return -1;
  }

  if (listen(server_socket_fd, QUEUE_LIMIT) < 0)
  {
    perror("Error on listening");
    return -1;
  }

  return server_socket_fd;
}

int accept_new_connection(int server_socket_fd)
{
  // NEW CONNECTION
  struct sockaddr_in addr;
  socklen_t addr_length = sizeof(addr);

  int client_socket_fd = accept(server_socket_fd, (struct sockaddr*) &addr, &addr_length);

  if (client_socket_fd < 0)
  {
    close(server_socket_fd);
    perror("Error on Accept client");
    return -1;
  }
  printf("Client connection accepted!\n");

  return client_socket_fd;
}

int handle_epoll_events(int epoll_fd, int server_socket_fd, struct epoll_event *client_events, int number_of_events,
                        int block_flag)
{
  int return_value = 0;
  uint8_t TI = 0;
  char buff[MAXBUF];

  for (int n = 0; n < number_of_events; ++n)
  {
    if (client_events[n].data.fd == server_socket_fd)
    {
      int client_socket_fd = accept_new_connection(server_socket_fd);

      return_value = set_socket_to_non_block(client_socket_fd, block_flag);

      if (return_value < 0)
      {
        perror("Error on set_socket_non_block");
        break;
      }
      //adding event to list
      return_value = epoll_control(epoll_fd, client_socket_fd, EPOLLIN, EPOLL_CTL_ADD);

      if (return_value < 0)
      {
        perror("Error o epoll_control");
        break;
      }
    }
    else
    {
      //CURRENT CONNECTION
      if (client_events[n].events & EPOLLIN)
      {
        printf("Server received EPOLLIN! \n");

        int fd = client_events[n].data.fd;
        int received_bytes = 0;
        memset(buff, 0, MAXBUF);

        received_bytes = recv_frame(fd, buff);

        if (received_bytes > 0)
        {
          frame frame = string_to_frame(buff);
          frameInterpretation(frame);
        }
        else
        {
          printf("No data received on server!\n");
        }

        return_value = epoll_control(epoll_fd, fd, EPOLLOUT, EPOLL_CTL_MOD);

        if (return_value < 0)
        {
          perror("Error on epoll_control");
          break;
        }
      }
      else if (client_events[n].events & EPOLLOUT)
      {
        printf("received EPOLLOUT! \n");
        int fd = client_events[n].data.fd;

        frame radioConditionStatus;
        char *data = calloc(sizeof(char), 4);
        data[0] = 0x38;
        data[1] = 0xA7;
        data[2] = 0xB4;
        data[3] = 0x3C;//to use find_radio_conditions here

        radioConditionStatus = generateFrame(11,0,0,0,0,0,0,data,0);

        //GENERATE PADDING START
        int length = strlen(data) * BITS_IN_BYTE;
        uint16_t dataSizeBytes = length / BITS_IN_BYTE;
        uint16_t frameData_size = calculate_dataFrame_size(length) / BITS_IN_BYTE;
        uint16_t padding_size = frameData_size - dataSizeBytes;


        radioConditionStatus.dataFrame.frameData.padding = calloc(sizeof(char), padding_size);
        //GENERATE PADDING END
        //this should be done in function, maybe as part generateFrame?

        uint16_t bufferSize;
        unsigned char *buff = frameIntoBuffer(radioConditionStatus, &bufferSize);

        printf("Data size %uB, Padding Size %dB\n", dataSizeBytes, padding_size);
        printf("Sending frame of size %uB to client %d\n", bufferSize, fd);

        int bytes_sent = send_frame(fd, buff, bufferSize);

        if (bytes_sent < 0)
        {
          printf("Error on sending frame");
        }

        return_value = epoll_control(epoll_fd, fd, EPOLLIN, EPOLL_CTL_MOD);

        if (return_value < 0)
        {
          perror("Error on epoll_ctl");
          break;
        }
      }
      else
      {
        printf("Unknown event received!\n");
        return_value = -1;
      }
    }
  }

  return return_value;
}

int main_event_loop(void)
{
  int return_value = -1;
  struct epoll_event client_events[QUEUE_LIMIT];

  //PREPARING SOCKET TO USE
  int server_socket_fd = socket_handle(AF_INET, INADDR_ANY, SOL_SOCKET, SOCK_STREAM);

  if (server_socket_fd < 0)
  {
    perror("Error on socket_handle in main_event_loop");
    return return_value;
  }

  //NOT BLOCKING
  set_socket_to_non_block(server_socket_fd, F_SETFL);

  int epoll_fd = epoll_create(0);

  //adds fd to interest list
  return_value = epoll_control(epoll_fd, server_socket_fd, EPOLLIN, EPOLL_CTL_ADD);
  if (return_value < 0)
  {
    perror("Error on epoll_control main_event_loop");
    return return_value;
  }

  while (true)
  {
    printf("\nSERVER...\n");
    //waits for events on epoll
    int number_of_events = epoll_wait(epoll_fd, client_events, QUEUE_LIMIT, -1);

    if (number_of_events < 0)
    {
      perror("Error on wait epoll");
      return_value = -1;
      break;
    }

    return_value = handle_epoll_events(epoll_fd, server_socket_fd, client_events, number_of_events, F_SETFL);

    if (return_value < 0)
    {
      perror("Error on handle_epoll_events in main_event_loop");
      //break;
    }
  }

  return return_value;
}

