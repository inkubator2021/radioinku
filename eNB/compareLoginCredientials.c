#include "compareLoginCredientials.h"

#define MAXBUF 1024
#define DIV_CHAR " "

// Function returns:
// -1 - unable to open users.txt
// -2 - wrong password
// -3 - given login not found in users.txt
//  1 - logged succesfully

int compareLoginCredientials(char login[], char password[], char file[])
{
    char loginBuf[credientialLength];
    char passwordBuf[credientialLength];
    FILE* fp = fopen(file, "r");
    if (!fp)
        return fileError;

    int counter = 0;
    while(fgets(loginBuf, sizeof(loginBuf), fp))
    {
        if(!(counter % 2))
        {
            loginBuf[strcspn(loginBuf, "\n")] = eraseCharacter;
            if(!strcmp(login, loginBuf))
            {
                fgets(passwordBuf, sizeof(passwordBuf), fp);
                passwordBuf[strcspn(passwordBuf, "\n")] = eraseCharacter;
                if(!strcmp(password, passwordBuf))
                {
                    fclose(fp);
                    return successfulLogin;
                }
                else
                {
                    fclose(fp);
                    return passwordError;
                }
            }
        }
        counter++;
    }
    fclose(fp);
    return loginError;
}

int parseLoginData(unsigned char *loginData, unsigned char **login, unsigned char **password)
{
  int retval = -1;
  if (loginData == NULL)
  {
    return retval;
  }

  *login = (char*) calloc(MAXBUF, sizeof(char));
  *password = (char*) calloc(MAXBUF, sizeof(char));
  unsigned char *token;

  token = strtok(loginData, DIV_CHAR);

  if (token != NULL)
  {
    memcpy(*login, token, strlen(token));
    token = strtok(NULL, DIV_CHAR);
    if (token != NULL)
    {
      memcpy(*password, token, strlen(token));
      retval = 1;
    }
    else
    {
      retval = -1;
    }
  }
  else
  {
    retval = -1;
  }

  return retval;
}
