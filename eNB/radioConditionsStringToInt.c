#include "radioConditionsStringToInt.h"

uint32_t radioConditionsStringToInt(unsigned char *radioConditionsString)
{
    uint32_t radioConditionsNumber = 0;
    uint32_t temp = 0;
    for (int i = 0; i < RADIO_COND_MESSAGE_SIZE_IN_BYTES; i++)
    {
        temp = radioConditionsString[i] << (RADIO_COND_MESSAGE_SIZE_IN_BYTES - i - 1) * CHAR_BIT;
        radioConditionsNumber |= temp;
        temp = 0;
    }
    return radioConditionsNumber;
}