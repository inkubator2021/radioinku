#ifndef __ENB
#define __ENB

#define PORT 8080
#define MAXBUF 1024
#define QUEUE_LIMIT 5

#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <errno.h>
#include <strings.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdbool.h>
#include <fcntl.h>

#include "epollFunctions.h"
#include "frameStructure.h"
#include "frameSerialization.h"
#include "string_to_frame.h"
#include "dataFrameSize.h"

int socket_handle(int net_flag, int addr_flag, int socket_lvl, int type);

int accept_new_connection(int server_socket_fd);

int handle_epoll_events(int epoll_fd, int server_socket_fd, struct epoll_event *client_events,
			int number_of_events, int block_flag);

int main_event_loop(void);


#endif // __ENB
