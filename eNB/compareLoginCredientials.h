#ifndef COMPARE_LOGIN_CREDIENTIALS_H
#define COMPARE_LOGIN_CREDIENTIALS_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define credientialLength 100
#define eraseCharacter 0
#define fileError -1
#define passwordError -2
#define loginError -3
#define successfulLogin 1

int compareLoginCredientials(char login[], char password[], char file[]);

int parseLoginData(unsigned char *loginData, unsigned char **login, unsigned char **password);

#endif
