#include "find_radio_conditions.h"

int find_radio_conditions (uint32_t testData)
{
    uint32_t dif = CONDITION_DATA ^ testData;
    int radio_conditions = 1;
    for (int i = 0; i < 32; i++)
    {
        if (dif & 1)
        {
            radio_conditions++;
        }
        dif = dif >> 1;
    }

    return radio_conditions;
}