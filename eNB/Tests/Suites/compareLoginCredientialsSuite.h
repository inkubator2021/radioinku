#ifndef COMPARE_LOGIN_CREDIENTIALS_SUITE
#define COMPARE_LOGIN_CREDIENTIALS_SUITE

#include "compareLoginCredientials.h"
#include <assert.h>

#define TAB_SIZE 5

void runCompareLoginCredientialsTests();
void testCompareLoginCredientials();
// void testOpeningWrongFile();
//int mockFopen(const char *restrict pathname, const char *restrict mode);

#endif
