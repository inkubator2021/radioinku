#include "codingLevelSuite.h"

//---------------------------------------------------------------
// PURPOSE: Verify codingLevel function - special case - RADIO_LEVEL_0
// "Radio conditions unknown" - error code
//
//PREREQUISITES:
//  a) None
//
//  TRIGGER:
//  1) calling function codingLevel with RADIO_LEVEL_0 arg
//
//  VERIFICATION:
//  1) function returns error value
//---------------------------------------------------------------
void codingLevelTest1()
{
  printf("%s:%d\n", __func__, __LINE__);
  int8_t ret = codingLevel(RADIO_LEVEL_0);

  assert(ret == -1);

  printf("Test %s passed\n\n",  __func__);
}

//---------------------------------------------------------------
// PURPOSE: Verify codingLevel function - special case - RADIO_LEVEL_MAX
// Radio conditions extremely bad
//
//PREREQUISITES:
//  a) None
//
//  TRIGGER:
//  1) calling function codingLevel with RADIO_LEVEL_MAX arg
//
//  VERIFICATION:
//  1) function returns 0 value with information about case
//---------------------------------------------------------------
void codingLevelTest2()
{
  printf("%s:%d\n", __func__, __LINE__);
  int8_t ret = codingLevel(RADIO_LEVEL_MAX);

  assert(ret == 0);

  printf("Test %s passed\n\n",  __func__);
}

//---------------------------------------------------------------
// PURPOSE: Verify codingLevel function - RADIO_LEVEL_1
//
//PREREQUISITES:
//  a) None
//
//  TRIGGER:
//  1) calling function codingLevel with RADIO_LEVEL_1 arg
//
//  VERIFICATION:
//  1) function returns 1(the lowest for this radio conditions) coding level
//---------------------------------------------------------------
void codingLevelTest3()
{
  printf("%s:%d\n", __func__, __LINE__);
  int8_t ret = codingLevel(RADIO_LEVEL_1);

  assert(ret == 1);

  printf("Test %s passed\n\n",  __func__);
}

//---------------------------------------------------------------
// PURPOSE: Verify codingLevel function - RADIO_LEVEL_3
//
//PREREQUISITES:
//  a) None
//
//  TRIGGER:
//  1) calling function codingLevel with RADIO_LEVEL_3 arg
//
//  VERIFICATION:
//  1) function returns 2(the lowest for this radio conditions) coding level
//---------------------------------------------------------------
void codingLevelTest4()
{
  printf("%s:%d\n", __func__, __LINE__);
  int8_t ret = codingLevel(RADIO_LEVEL_3);

  assert(ret == 2);

  printf("Test %s passed\n\n",  __func__);
}

//---------------------------------------------------------------
// PURPOSE: Verify codingLevel function - RADIO_LEVEL_5
//
//PREREQUISITES:
//  a) None
//
//  TRIGGER:
//  1) calling function codingLevel with RADIO_LEVEL_5 arg
//
//  VERIFICATION:
//  1) function returns 3(the lowest for this radio conditions) coding level
//---------------------------------------------------------------
void codingLevelTest5()
{
  printf("%s:%d\n", __func__, __LINE__);
  int8_t ret = codingLevel(RADIO_LEVEL_5);

  assert(ret == 3);

  printf("Test %s passed\n\n",  __func__);
}

//---------------------------------------------------------------
// PURPOSE: Verify codingLevel function - RADIO_LEVEL_MOCK
//
//PREREQUISITES:
//  a) None
//
//  TRIGGER:
//  1) calling function codingLevel with RADIO_LEVEL_MOCK arg
//
//  VERIFICATION:
//  1) function returns error code
//---------------------------------------------------------------
#define RADIO_LEVEL_MOCK 18
void codingLevelTest6()
{
  printf("%s:%d\n", __func__, __LINE__);
  int8_t ret = codingLevel(RADIO_LEVEL_MOCK);

  assert(ret == -1);

  printf("Test %s passed\n\n",  __func__);
}

void codingLevelTests()
{
  codingLevelTest1();
  codingLevelTest2();
  codingLevelTest3();
  codingLevelTest4();
  codingLevelTest5();
  codingLevelTest6();
}

