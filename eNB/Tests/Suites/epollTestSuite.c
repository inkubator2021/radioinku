#include "epollTestSuite.h"
#include "eNBTestsWrappers.h"

extern bool mock_accept;
extern bool mock_socket;
extern bool mock_epoll_ctl;
extern bool epoll_wait_success;

#define number_of_events 1

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify socket_handle function

//PREREQUISITIES:
//a) Passing valid arguments to function

//TRIGGER:
//1) Running socket_handle function with valid arguments

//VERIFICATION
//1) Verify creating proper socket fd by function socket
//2) Verify setsockopt function - reusing address and port
//2) Verify binding socket with server address - function bind
//4) Verify if listen function not returning error
//-------------------------------------------------------------------------------------------------
void test_1_socket_handle(void)
{
  printf("%s:%d\n", __func__, __LINE__);

  int fd = socket_handle(AF_INET, INADDR_ANY, SOL_SOCKET, SOCK_STREAM);
  assert(fd >= 0);
  printf("Positive test test_1_socket_create_bind passed\n\n");

}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify system function socket in function socket_handle

//PREREQUISITIES:
//a) None

//TRIGGER:
//1) Running socket_handle function with invalid net_flag argument

//VERIFICATION
//1) Verify error handling - function socket - function should return error code
//-------------------------------------------------------------------------------------------------
#define net_flag -1
void test_2_socket_handle(void)
{
  printf("%s:%d\n", __func__, __LINE__);

  int fd = socket_handle( -1, INADDR_ANY, SOL_SOCKET, SOCK_STREAM);
  assert(fd == -1);
  printf("Test test_2_socket_create passed\n\n");

}
#undef net_flag

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify system function bind in function socket_handle

//PREREQUISITIES:
//a) socket fd should exist

//TRIGGER:
//1) Running socket_handle function with invalid addr_flag argument

//VERIFICATION
//1) Verify error handling - function bind - function should return error code
//-------------------------------------------------------------------------------------------------
#define addr_flag 1
void test_3_socket_handle(void)
{
  printf("%s:%d\n", __func__, __LINE__);

  int fd = socket_handle(AF_INET, 1, SOL_SOCKET, SOCK_STREAM);
  assert(fd == -1);
  printf("Test test_2_socket_bind passed\n\n");
}
#undef socket_lvl

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function setsockopt in function socket_handle

//PREREQUISITIES:
//a) Socket fd should exist

//TRIGGER:
//1) Running socket_handle function with invalid socket_lvl argument

//VERIFICATION
//1) Verify error handling - function setsockopt - function should return error code
//-------------------------------------------------------------------------------------------------
#define socket_lvl -1
void test_4_socket_handle(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  int fd = socket_handle(AF_INET, INADDR_ANY, SOL_SOCKET, SOCK_STREAM);
  fd = socket_handle(AF_INET, INADDR_ANY, -1, SOCK_STREAM);
  assert(fd == -1);
  printf("Test test_4_socket_create_bind: setsockopt passed\n\n");
}
#undef socket_lvl

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function listen in function socket_handle

//PREREQUISITIES:
//a) Server socket is created and properly bound to server adddress

//TRIGGER:
//1) Running socket_handle function with invalid protocol argument

//VERIFICATION
//1) Verify function listen error handling - function should return -1
//-------------------------------------------------------------------------------------------------
#define PROTOCOL SOCK_DGRAM
void test_5_socket_handle(void)
{
  printf("%s:%d\n", __func__, __LINE__);

  int fd = socket_handle(AF_INET, INADDR_ANY, SOL_SOCKET, PROTOCOL);

  assert(fd == -1);

  printf("Test test_5_socket_handle passed\n\n");
}
#undef PROTOCOL

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function handle_epoll_events - new event positive sceanrio

//PREREQUISITIES:
//a) Server socket is created and set up by function socket_handle and set_socket_to_non_block
//b) Structure epoll_event of client_events and epoll instance are created
//c) Server socket fd is assigned to first client_events.data.fd element

//TRIGGER:
//1) Running handle_epoll_events function

//VERIFICATION
//1) Function accept_new_connection returns proper client_socket_fd
//2) Function set_socket_to_non_block successfully setting socket to not block transmissions
//3) Function epoll_control function successfully adds entry to epoll interest list
//4) Function handle_epoll_events return value is >= 0.
//-------------------------------------------------------------------------------------------------
void test_1_handle_epoll_events(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  struct epoll_event client_events[QUEUE_LIMIT];

  int server_socket_fd = socket_handle(AF_INET, INADDR_ANY, SOL_SOCKET, SOCK_STREAM);
  set_socket_to_non_block(server_socket_fd, F_SETFL);
  int epoll_fd = epoll_create(0);
  int return_value = epoll_control(epoll_fd, server_socket_fd, EPOLLIN, EPOLL_CTL_ADD);

  client_events[0].data.fd = server_socket_fd;

  int ret = handle_epoll_events(epoll_fd, server_socket_fd, client_events,
                                number_of_events, F_SETFL);
  assert(ret >= 0);
  printf("Test test_1_handle_epoll_events passed\n\n");
}
//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function handle_epoll_events - set_socket_to_non_block

//PREREQUISITIES:
//a) Server socket is created and set up by function socket_handle
//b) Structure epoll_event of client_events and epoll instance are created
//c) Server socket fd is assigned to first client_events.data.fd element
//d) Function accept_new_connection returns proper client_socket_fd

//TRIGGER:
//1) Running handle_epoll_events function

//VERIFICATION
//1) Function set_socket_to_non_block fail in setting socket to not block transmissions
//2) Function handle_epoll_events return value is -1.
//-------------------------------------------------------------------------------------------------
#define block_flag -18
void test_2_handle_epoll_events(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  struct epoll_event client_events[QUEUE_LIMIT];
  int server_socket_fd = socket_handle(AF_INET, INADDR_ANY, SOL_SOCKET, SOCK_STREAM);
  set_socket_to_non_block(server_socket_fd, F_SETFL);
  int epoll_fd = epoll_create(0);
  int return_value = epoll_control(epoll_fd, server_socket_fd, EPOLLIN, EPOLL_CTL_ADD);

  client_events[0].data.fd = server_socket_fd;

  int ret = handle_epoll_events(epoll_fd, server_socket_fd, client_events, number_of_events, block_flag);
  assert(ret == -1);
  printf("Test test_2_handle_epoll_events passed\n\n");
}
#undef block_flag

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function handle_epoll_events - epoll_control

//PREREQUISITIES:
//a) Server socket is created and set up by function socket_handle and set_socket_to_non_block
//b) Structure epoll_event of client_events is created
//c) Server socket fd is assigned to first client_events.data.fd element
//d) Function accept_new_connection returns proper client_socket_fd
//e) Function set_socket_to_non_block successfully setting socket to not block transmissions

//TRIGGER:
//1) Running handle_epoll_events function with invalid epoll_fd

//VERIFICATION
//1) Function epoll_control function fail to add entry to epoll interest list
//2) Function handle_epoll_events return value is -1.
//-------------------------------------------------------------------------------------------------
#define epoll_fd -1
void test_3_handle_epoll_events(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  struct epoll_event client_events[QUEUE_LIMIT];
  int server_socket_fd = socket_handle(AF_INET, INADDR_ANY, SOL_SOCKET, SOCK_STREAM);

  client_events[0].data.fd = server_socket_fd;

  int ret = handle_epoll_events(epoll_fd, server_socket_fd, client_events, number_of_events, F_SETFL);
  assert(ret == -1);
  printf("Test test_3_handle_epoll_events passed\n\n");
}
#undef epoll_fd

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function handle_epoll_events - existing event(EPOLLIN) - epoll_control

//PREREQUISITIES:
//a) Server socket is created and set up by function socket_handle and set_socket_to_non_block
//b) Structure epoll_event of client_events is created
//c) Creating mock_socket_fd to enter path for existing events in loop
//d) Setting client_events[0].events on EPOLLIN to enter path for receiving message in loop

//TRIGGER:
//1) Running handle_epoll_events function with invalid epoll_fd argument and mock_socket_fd

//VERIFICATION
//1) Function epoll_control function fail to modify event in epoll interest list for particular fd
//2) Function handle_epoll_events return value is -1.
//-------------------------------------------------------------------------------------------------
#define epoll_fd -1
#define mock_socket_fd -1
void test_4_handle_epoll_events(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  struct epoll_event client_events[QUEUE_LIMIT];
  int server_socket_fd = socket_handle(AF_INET, INADDR_ANY, SOL_SOCKET, SOCK_STREAM);

  client_events[0].data.fd = server_socket_fd;
  client_events[0].events = EPOLLIN;

  int ret = handle_epoll_events(epoll_fd, mock_socket_fd, client_events, number_of_events, F_SETFL);
  assert(ret == -1);
  printf("Test test_4_handle_epoll_events passed\n\n");
}
#undef epoll_fd
#undef mock_socket_fd

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function handle_epoll_events - existing event(EPOLLOUT) - epoll_control

//PREREQUISITIES:
//a) Server socket is created and set up by function socket_handle and set_socket_to_non_block
//b) Structure epoll_event of client_events is created
//c) Creating mock_socket_fd to enter path for existing events in loop
//d) Setting client_events[0].events on EPOLLOUT to enter path for sending message in loop
//e) Wrapping system send function (in send_rsp) into __wrap_send to make it return proper value

//TRIGGER:
//1) Running handle_epoll_events function with invalid epoll_fd argument and mock_socket_fd

//VERIFICATION
//1) Function epoll_control function fail to modify event in epoll interest list for particular fd
//2) Function handle_epoll_events return value is -1.
//-------------------------------------------------------------------------------------------------
#define epoll_fd -1
#define mock_socket_fd -1
void test_5_handle_epoll_events(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  struct epoll_event client_events[QUEUE_LIMIT];
  int server_socket_fd = socket_handle(AF_INET, INADDR_ANY, SOL_SOCKET, SOCK_STREAM);
  set_socket_to_non_block(server_socket_fd, F_SETFL);

  client_events[0].data.fd = server_socket_fd;
  client_events[0].events = EPOLLOUT;

  int ret = handle_epoll_events(epoll_fd, mock_socket_fd, client_events, number_of_events, F_SETFL);
  assert(ret == -1);
  printf("Test test_5_handle_epoll_events passed\n\n");
}
#undef epoll_fd
#undef mock_socket_fd

//PURPOSE: Verify function main_event_loop - socket_handle failure

//PREREQUISITIES:
//a) None

//TRIGGER:
//1) Running handle_epoll_events function

//VERIFICATION
//1) Using __wrap_socket function instead of socket to check function socket_handle error handling
//2) Function main_event_loop return value is -1.
//-------------------------------------------------------------------------------------------------
void test_1_main_event_loop(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  int ret;
  mock_socket = true;
  ret = main_event_loop();
  assert(ret == -1);
  mock_socket = false;
  printf("Test test_1_main_event_loop passed\n\n");
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function main_event_loop - epoll_control failure

//PREREQUISITIES:
//a) Mock epoll_ctl

//TRIGGER:
//1) Running main_event_loop function

//VERIFICATION
//1) Server socket is created and set up by function socket_handle and set_socket_to_non_block
//2) Structure epoll_event of client_events and epoll instance are created
//3) Function epoll_control returns error code
//4) Function main_event_loop return value is -1.
//-------------------------------------------------------------------------------------------------
void test_2_main_event_loop(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  int ret;
  mock_epoll_ctl = true;
  ret = main_event_loop();
  assert(ret == -1);
  mock_epoll_ctl = false;
  printf("Test test_4_main_event_loop passed\n\n");
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function main_event_loop - epoll_wait failure

//PREREQUISITIES:
//a) Mock epoll_wait

//TRIGGER:
//1) Running main_event_loop function

//VERIFICATION
//1) Server socket is created and set up by function socket_handle and set_socket_to_non_block
//2) Structure epoll_event of client_events and epoll instance are created
//3) Function epoll_control sucessfully adds socket fd to interest list
//4) Function epoll_wait returns error code
//5) Function main_event_loop return value is -1.
//-------------------------------------------------------------------------------------------------
void test_3_main_event_loop(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  epoll_wait_success = false;
  int ret;
  ret = main_event_loop();
  assert(ret == -1);
  epoll_wait_success = true;
  printf("Test test_2_main_event_loop passed\n\n");
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function main_event_loop - handle_epoll_events failure

//PREREQUISITIES:
//a) Mock epoll_wait

//TRIGGER:
//1) Running main_event_loop function

//VERIFICATION
//1) Server socket is created and set up by function socket_handle and set_socket_to_non_block
//2) Structure epoll_event of client_events and epoll instance are created
//3) Function epoll_control successfully adds socket fd to interest list
//4) Function epoll_wait filling client_events.events with EPOLLET flag -
//   handle_epoll_events returns error code
//5) Function main_event_loop return value is -1.
//-------------------------------------------------------------------------------------------------
void test_4_main_event_loop(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  int ret;
  ret = main_event_loop();
  assert(ret == -1);
  printf("Test test_1_main_event_loop passed\n\n");
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function accept_new_connection - failure

//PREREQUISITIES:
//a) Mock epoll_wait

//TRIGGER:
//1) Running accept_new_connection function

//VERIFICATION
//1) Server socket is created and set up by function socket_handle and set_socket_to_non_block
//2) Function accept_connection return value is -1.
//-------------------------------------------------------------------------------------------------

void test_1_accept_new_connection(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  mock_accept = false;
  int fd = socket_handle(AF_INET, INADDR_ANY, SOL_SOCKET, SOCK_STREAM);
  assert(fd >= 0);
  set_socket_to_non_block(fd, F_SETFL);
  int ret = accept_new_connection(fd);
  assert(ret < 0);
  printf("Test test_1_accept_new_connection passed\n\n");
}

void epoll_tests(void)
{
  test_1_socket_handle();
  test_2_socket_handle();
  test_3_socket_handle();
  test_4_socket_handle();
  test_5_socket_handle();

  test_1_handle_epoll_events();
  test_2_handle_epoll_events();
  test_3_handle_epoll_events();
  test_4_handle_epoll_events();
  test_5_handle_epoll_events();

  test_1_main_event_loop();
  test_2_main_event_loop();
  test_3_main_event_loop();

  test_1_accept_new_connection();
}

