#include "compareLoginCredientialsSuite.h"

//----------------------------------------------------------------------------
// PURPOSE:Verify testCheckIfNumberIsRepeated function
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) function checks if given login credientials are correct
//  2) function returns -3 when given login is not found in file 
//  3) function returns -2 when given password doesn't match password in file
//  4) function returns -1 when it's unable to open file
//  5) function returns  1 when provided login credientials are correct
//-----------------------------------------------------------------------------

void testCompareLoginCredientials()
{
  assert(compareLoginCredientials("admin", "admin", "eNB/users.txt") == 1);
  assert(compareLoginCredientials("bartek", "blednehaslo", "eNB/users.txt") == -2);
  assert(compareLoginCredientials("Mariusz", "abcd", "eNB/users.txt") == -3);
  assert(compareLoginCredientials("admin", "admin", "wrongfile.txt") == -1);
}

//----------------------------------------------------------------------------
// PURPOSE:Verify parseLoginData function is working with valid argument
//
//  PREQUISITES:
//  a) Passing valid loginData to function(login and password with whitespace between)
//
//  TRIGGER:
//  1) calling function
//
//  VERIFICATION:
//  1)Function parseLoginData returns 1
//-----------------------------------------------------------------------------
void parseLoginDataTest1()
{
  printf("%s:%d\n", __func__, __LINE__);

  unsigned char loginData[] = "ola pola";
  unsigned char *login;
  unsigned char *password;
  int ret = parseLoginData(loginData, &login, &password);

  assert(ret = 1);

  free(login);
  free(password);
  printf("Test %s passed\n\n", __func__);
}

//----------------------------------------------------------------------------
// PURPOSE:Verify parseLoginData function is returning error code with
// argument without whitespace
//
//  PREQUISITES:
//  a) Passing invalid loginData to function(argument without whitespace)
//
//  TRIGGER:
//  1) calling function
//
//  VERIFICATION:
//  1)Function parseLoginData returns -1
//-----------------------------------------------------------------------------
void parseLoginDataTest2()
{
  printf("%s:%d\n", __func__, __LINE__);

  unsigned char loginData[] = "ola";
  unsigned char *login;
  unsigned char *password;
  int ret = parseLoginData(loginData, &login, &password);

  assert(ret = -1);
  free(login);
  free(password);
  printf("Test %s passed\n\n", __func__);
}

//----------------------------------------------------------------------------
// PURPOSE:Verify parseLoginData function is returning error code in situation
// when strtok function returns NULL
//
//  PREQUISITES:
//  a) Passing invalid loginData to function(only whitespace)
//
//  TRIGGER:
//  1) calling function
//
//  VERIFICATION:
//  1)Function parseLoginData returns -1
//-----------------------------------------------------------------------------
void parseLoginDataTest3()
{
  printf("%s:%d\n", __func__, __LINE__);

  char loginData[]=" ";
  unsigned char *login;
  unsigned char *password;
  int ret = parseLoginData(loginData, &login, &password);

  assert(ret = -1);
  free(login);
  free(password);
  printf("Test %s passed\n\n", __func__);
}

//----------------------------------------------------------------------------
// PURPOSE:Verify parseLoginData function is returning error code in situation
// when passing pointer argument is set to NULL
//
//  PREQUISITES:
//  a) Passing invalid loginData to function(pointer set to NULL)
//
//  TRIGGER:
//  1) calling function
//
//  VERIFICATION:
//  1)Function parseLoginData returns -1
//-----------------------------------------------------------------------------
void parseLoginDataTest4()
{
  printf("%s:%d\n", __func__, __LINE__);

  char *loginData = NULL;
  unsigned char *login;
  unsigned char *password;
  int ret = parseLoginData(loginData, &login, &password);

  assert(ret = -1);
  printf("Test %s passed\n\n", __func__);

}

void runCompareLoginCredientialsTests()
{
  testCompareLoginCredientials();
  parseLoginDataTest1();
  parseLoginDataTest2();
  parseLoginDataTest3();
  parseLoginDataTest4();
}

