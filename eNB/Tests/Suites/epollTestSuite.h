#ifndef EPOLL_TEST_SUITE
#define EPOLL_TEST_SUITE

#include "enbEventLoop.h"
#include "epollFunctions.h"

void test_1_socket_handle(void);
void test_2_socket_handle(void);
void test_3_socket_handle(void);
void test_4_socket_handle(void);
void test_5_socket_handle(void);
void test_1_handle_epoll_events(void);
void test_2_handle_epoll_events(void);
void test_3_handle_epoll_events(void);
void test_4_handle_epoll_events(void);
void test_5_handle_epoll_events(void);
void test_1_main_event_loop(void);
void test_2_main_event_loop(void);
void test_3_main_event_loop(void);
void test_4_main_event_loop(void);
void test_1_accept_new_connection(void);
void epoll_tests(void);

#endif
