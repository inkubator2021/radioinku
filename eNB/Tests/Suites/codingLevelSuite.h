#ifndef __CODING_LEVEL_TEST
#define __CODING_LEVEL_TEST

#include "codingLevel.h"
#include <assert.h>

void codingLevelTest1(void);
void codingLevelTest2(void);
void codingLevelTest3(void);
void codingLevelTest4(void);
void codingLevelTest5(void);
void codingLevelTest6(void);

void codingLevelTests(void);

#endif
