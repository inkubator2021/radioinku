#include "findRadioConditionsSuite.h"

//---------------------------------------------------------------
// PURPOSE:Verify find radio conditions function
//
//  PREQUISITES:
//  None
//
//  TRIGGER:
//  1) running function with CONDITION_DATA as an argument
//
//  VERIFICATION:
//  1) Function returns 1
//---------------------------------------------------------------

void test_find_radio_coditions_1()
{
    printf("%s:%d\n", __func__, __LINE__);
    assert(find_radio_conditions(CONDITION_DATA) == 1);
    printf("%s done\n", __func__);
}

//---------------------------------------------------------------
// PURPOSE:Verify find radio conditions function
//
//  PREQUISITES:
//  a) uint32_t variables with one bit different than CONDITION_DATA
//
//  TRIGGER:
//  1) running function with argument from a)
//
//  VERIFICATION:
//  1) Function returns 2
//---------------------------------------------------------------

void test_find_radio_coditions_2()
{
    printf("%s:%d\n", __func__, __LINE__);
    uint32_t a = 0b00111000101001111011010000111101;
    assert(find_radio_conditions(a) == 2);
    printf("%s done\n", __func__);
}

void run_cond_tests()
{
    test_find_radio_coditions_1();
    test_find_radio_coditions_2();
}
