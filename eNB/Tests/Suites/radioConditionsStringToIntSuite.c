#include "radioConditionsStringToIntSuite.h"

//---------------------------------------------------------------
// PURPOSE:Verify radioConditionStringToIntTest function
//
//  PREREQUISITES:
//  a) unsigned char* with 4 chars
//
//  TRIGGER:
//  1) running function with argument from a) and check if equal to
//  number with variable a) values from ASCII set on corresponding bytes
//
//  VERIFICATION:
//  1) Assertion returns true
//---------------------------------------------------------------
void radioConditionStringToIntTest()
{
    printf("%s:%d\n", __func__, __LINE__);
    unsigned char* text = "koty";
    assert(radioConditionsStringToInt(text) == 0b01101011011011110111010001111001);
    printf("%s done\n", __func__);
}