#include <assert.h>
#include <stdlib.h>
#include <stdio.h>

#include "compareLoginCredientialsSuite.h"
#include "findRadioConditionsSuite.h"
#include "epollTestSuite.h"
#include "radioConditionsStringToIntSuite.h"
#include "codingLevelSuite.h"


int main()
{
    epoll_tests();
    runCompareLoginCredientialsTests();
    run_cond_tests();
    radioConditionStringToIntTest();
    codingLevelTests();

    return 0;
}
