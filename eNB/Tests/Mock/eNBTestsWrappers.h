#ifndef ENB_TESTS_WRAPPERS
#define ENB_TESTS_WRAPPERS

#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <strings.h>
#include <unistd.h>
#include <stdbool.h>

//#include "srv_func.h"
//#include "shared_func.h"

bool mock_accept = true;
bool mock_socket = false;
bool mock_epoll_ctl = false;
bool epoll_wait_success = true;

int __real_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen);
int __wrap_accept(int sockfd, struct sockaddr *addr, socklen_t *addrlen)
{
  //printf("Calling mocked accept function.\n");
  if(mock_accept == true)
  {
    int client_socket_fd = socket(AF_INET, SOCK_STREAM, 0);
    printf("test accept...%d\n",client_socket_fd);
    printf("Client connection mock_accepted!\n");

    return client_socket_fd;
  }
  else
  {
    return __real_accept(sockfd, addr, addrlen);
  }
}

int __wrap_send(int sockfd, const void *buf, size_t len, int flags)
{
    return sockfd;
}

int __wrap_epoll_wait(int epfd, struct epoll_event *events, int maxevents, int timeout)
{
    events->events = EPOLLET;
    if(epoll_wait_success == true)
    {
      maxevents = 1;
    }
    else
    {
      maxevents = -1;
    }
    return maxevents;
}

int __real_epoll_ctl(int epfd, int op, int fd, struct epoll_event *event);
int __wrap_epoll_ctl(int epfd, int op, int fd, struct epoll_event *event)
{
    int ret_val = -1;
    if(mock_epoll_ctl == true)
    {
      return ret_val;
    }
    else
    {
      return __real_epoll_ctl(epfd, op, fd, event);
    }
}

int __real_socket(int domain, int type, int protocol);
int __wrap_socket(int domain, int type, int protocol)
{
  int ret_val = -1;
  if(mock_socket == true)
  {
    return ret_val;
  }
  else
  {
    return __real_socket(domain, type, protocol);
  }
}
#endif
