#ifndef __FIND_RADIO_COND
#define __FIND_RADIO_COND

#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#define CONDITION_DATA 0b00111000101001111011010000111100

int find_radio_conditions(uint32_t testData);

#endif // !__FIND_RADIO_COND