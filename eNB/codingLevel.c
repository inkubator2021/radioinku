#include "codingLevel.h"

//this function is only used for frame with TI 11 , thus only on enb side
int8_t codingLevel(uint8_t radioConditions)
{
  if (radioConditions == RADIO_LEVEL_0)
  {
    printf("Radio conditions unknown");
    return -1;
  }
  else if(radioConditions == RADIO_LEVEL_MAX)
  {
    printf("Radio conditions extremely bad, retransmission needed");
    return 0;
  }

  uint8_t codingLevel;

  printf("radioConditions %u\n", radioConditions);

  switch(radioConditions)
  {
    case RADIO_LEVEL_1:
    case RADIO_LEVEL_2:
      codingLevel = 1;
      break;

    case RADIO_LEVEL_3:
    case RADIO_LEVEL_4:
      codingLevel = 2;
      break;

    case RADIO_LEVEL_5:
    case RADIO_LEVEL_6:
      codingLevel = 3;
      break;

    default:
      printf("Wrong radioConditionsNumber\n");
      codingLevel = -1;
      break;
  }

  return codingLevel;
}
