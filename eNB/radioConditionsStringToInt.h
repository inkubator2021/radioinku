#ifndef  __COND_STR_TO_INT
#define __COND_STR_TO_INT

#include <stdlib.h>
#include <stdio.h>
#include <inttypes.h>
#include <string.h>
#include <limits.h>
#include "../UE/generateRadioCondAskFrame.h"
#include "find_radio_conditions.h"

#define RADIO_COND_MESSAGE_SIZE_IN_BYTES 4

uint32_t radioConditionsStringToInt(unsigned char *radioConditionsString);

#endif // ! __COND_STR_TO_INT