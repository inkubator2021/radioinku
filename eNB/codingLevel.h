#ifndef __FIND_CODING_LEVEL
#define __FIND_CODING_LEVEL

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>

#define RADIO_LEVEL_0 0
#define RADIO_LEVEL_1 1
#define RADIO_LEVEL_2 2
#define RADIO_LEVEL_3 3
#define RADIO_LEVEL_4 4
#define RADIO_LEVEL_5 5
#define RADIO_LEVEL_6 6
#define RADIO_LEVEL_MAX 7

int8_t codingLevel(uint8_t radioConditions);

#endif
