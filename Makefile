#HOW TO USE:
# make enb_bin 
# make ue_bin
# make test_enb
# make test_shared
# make test_ue
# make clean
CC = gcc
FLAGS := -lm#-g -pthread -Wpedantic -Wall -Werror -pedantic-errors
#MEM_FLAGS := 
TESTS_FLAGS_UE := -fprofile-arcs -ftest-coverage "-Wl,--wrap=epoll_wait,-wrap=epoll_ctl,-wrap=socket,-wrap=connect,--wrap=send_msg,--wrap=receive"
TESTS_FLAGS_ENB := -fprofile-arcs -ftest-coverage "-Wl,--wrap=accept,-wrap=send,-wrap=epoll_wait,-wrap=epoll_ctl,-wrap=socket"
TESTS_FLAGS_SHARED := -fprofile-arcs -ftest-coverage "-Wl,--wrap=epoll_ctl,-wrap=recv,-wrap=close"

#APPS
OBJ_ENB = enb.o
OBJ_UE = ue.o

#TESTS
ENB_TEST = enbTest.o
UE_TEST = ueTest.o
SHARED_TEST = sharedTest.o

INCS = -I Shared/
TEST_ENB_INCS = -I eNB/ -I eNB/Tests/Mock/ -I eNB/Tests/Suites/ 
TEST_UE_INCS = -I UE/ -I UE/Tests/Mock/ -I UE/Tests/Suites/ 
TEST_SHARED_INCS = -I eNB/ -I Shared/Tests/Mock/ -I Shared/Tests/Suites/

OUT_ENB = enb_bin
OUT_UE = ue_bin

OUT_TEST_ENB = test_enb
OUT_TEST_UE = test_ue
OUT_TEST_SHARED = test_shared


$(OUT_ENB): $(OBJ_ENB)
$(OUT_UE): $(OBJ_UE)

$(OUT_TEST_ENB): $(ENB_TEST)
$(OUT_TEST_UE): $(UE_TEST)
$(OUT_TEST_SHARED): $(SHARED_TEST)


#APPS
enb.o: $(wildcard eNB/*.c) $(wildcard Shared/*.c)
	$(CC) $(INCS) $^ -o $(OUT_ENB) $(FLAGS)
	
ue.o: $(wildcard UE/*.c) $(wildcard Shared/*.c)
	$(CC) $(INCS) $^ -o $(OUT_UE) $(FLAGS)

#TESTS
enbTest.o: $(filter-out eNB/enbApp.c, $(wildcard eNB/*.c)) $(wildcard Shared/*.c) $(wildcard eNB/Tests/*.c) $(wildcard eNB/Tests/Suites/*.c)
	$(CC) $(INCS) $(TEST_ENB_INCS) $^ -o $(OUT_TEST_ENB) $(TESTS_FLAGS_ENB) $(FLAGS)

sharedTest.o: $(wildcard Shared/Tests/Suites/*.c) $(wildcard Shared/*.c) $(wildcard Shared/Tests/*.c)
	$(CC) $(INCS) $(TEST_SHARED_INCS) $^ -o $(OUT_TEST_SHARED) $(TESTS_FLAGS_SHARED) $(FLAGS)
	
ueTest.o: $(filter-out UE/ueApp.c, $(wildcard UE/*.c)) $(wildcard Shared/*.c) $(wildcard UE/Tests/*.c) $(wildcard UE/Tests/Suites/*.c)
	$(CC) $(INCS) $(TEST_UE_INCS) $^ -o $(OUT_TEST_UE) $(TESTS_FLAGS_UE) $(FLAGS)

clean:
	-rm -rf *.gcov *.gcno *.html *.gcda *.o $(OUT_ENB) $(OUT_UE) $(OUT_TEST_ENB) $(OUT_TEST_SHARED) $(OUT_TEST_UE)

