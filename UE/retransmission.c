#include "retransmission.h"
#include <inttypes.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define TI_FOR_RADIO_COND_ASK 10
#define MAX_TRANSMISSION_NUMBER 2
#define MAX_CODING 3
#define CIPHERING_1 1
#define CIPHERING_2 2

frame generateRetransmissionFrame(frame prevFrame)
{
    // Header
    bool isLogged = false;
    bool isSubheaderB = false;
    int uid = 0;
    int tuid = 0;
    char *data;
    isLogged = prevFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.UID > 0 ? true : false;
    isSubheaderB = prevFrame.dataFrame.frameSubheader.frameSubheaderA.F > 0 ? true : false;

    int transmissionNumber = ++prevFrame.dataFrame.frameHeader.N;

    int ti, coding, ciphering;
    ti = prevFrame.dataFrame.frameHeader.TI;

    if (ti == TI_FOR_RADIO_COND_ASK)
    {
        // As we discussed, in case of NACK for ask for radio condition frame, try to send the same frame
        // Increment N only (done before)
        return prevFrame;
    }

    if (isLogged)
    {
        coding = prevFrame.dataFrame.frameControlElements.frameControlElementsLogged.C;
        ciphering = prevFrame.dataFrame.frameControlElements.frameControlElementsLogged.CC;
        uid = prevFrame.dataFrame.frameControlElements.frameControlElementsLogged.UID;
    }
    else
    {
        coding = prevFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.C;
        ciphering = prevFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.CC;
        tuid = prevFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.TUID;
    }

    if ((transmissionNumber == MAX_TRANSMISSION_NUMBER) && (coding < MAX_CODING))
    {
        coding++;
    }

    ciphering = (ciphering == CIPHERING_1) ? CIPHERING_2 : CIPHERING_1;

    int subheaderNum, isLast;

    if (!isSubheaderB)
    {
        subheaderNum = prevFrame.dataFrame.frameSubheader.frameSubheaderA.Num;
        isLast = prevFrame.dataFrame.frameSubheader.frameSubheaderA.L;
    }
    else
    {
        subheaderNum = prevFrame.dataFrame.frameSubheader.frameSubheaderB.Num;
        isLast = prevFrame.dataFrame.frameSubheader.frameSubheaderB.L;
    }

    data = prevFrame.dataFrame.frameData.data;

    frame newFrame = generateFrame(ti, transmissionNumber, uid, coding, tuid, subheaderNum, isLast, data, 0);
    // 0 for HSinfo_ack, unused

    if (isLogged)
    {
        newFrame.dataFrame.frameControlElements.frameControlElementsLogged.CC = ciphering;
    }
    else
    {
        newFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.CC = ciphering;
    }

    return newFrame;
}

#undef TI_FOR_RADIO_COND_ASK
#undef MAX_TRANSMISSION_NUMBER
#undef MAX_CODING
#undef CIPHERING_1
#undef CIPHERING_2