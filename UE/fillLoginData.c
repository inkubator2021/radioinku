#include "fillLoginData.h"

#define MAXBUF 1024
#define DIV_CHAR " "

unsigned char* fillLoginData(void)
{
  unsigned char *loginData = (char*) malloc(MAXBUF * sizeof(char));
  unsigned char password[MAXBUF];

  printf("Please enter your login:\n");
  scanf("%s", loginData);

  strcat(loginData, DIV_CHAR);

  printf("Please enter your password:\n");
  scanf("%s", password);

  strcat(loginData, password);

  printf("%s\n", loginData);

  return loginData;
}
