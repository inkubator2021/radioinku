#include "radioConditions.h"


uint32_t simulateRadioConditions()
{ 
    srand(time(NULL)); 
    uint32_t conditionData = 0b00111000101001111011010000111100;
    int randomPercentage = rand()%100+1;
    int numberOfBitsToChange = randomiseNumberOfBitsToChange(randomPercentage);
    int bitsToChange[numberOfBitsToChange];
    bool isNumberRepeated;
    int positionOfBitToChange;

    for (int i = 0; i < numberOfBitsToChange; i++)
    {
        do
        {
            isNumberRepeated = false;
            positionOfBitToChange = rand() % 32;
            isNumberRepeated = checkIfNumberIsInArray(positionOfBitToChange, bitsToChange, numberOfBitsToChange);
            if (isNumberRepeated == false) 
                bitsToChange[i] = positionOfBitToChange;
        } while (isNumberRepeated);
        conditionData ^= 1UL << positionOfBitToChange-1;    
    }
    printf("Number of bits to change: %d\n", numberOfBitsToChange);
    return conditionData;
}

bool checkIfNumberIsInArray(int numberToCheck, int *arrayToCompare, int arraySize)
{
    bool numberRepetitionFlag = false;
    for(int i = 0; i < arraySize; i++)
    {
        if(numberToCheck == arrayToCompare[i]) numberRepetitionFlag = true;
    }
    return numberRepetitionFlag;
}

int randomiseNumberOfBitsToChange(int percentage)
{
        switch(percentage)
    {
        case 1 ... 25:
            return 0;
        case 26 ... 45:
            return 1;
        case 46 ... 62:
            return 2;
        case 63 ... 75:
            return 3;
        case 76 ... 86:
            return 4;
        case 87 ... 94:
            return 5;
        case 95 ... 100:
            return 6;
        default:
            return -1;
    }
}

