#ifndef __GEN_ASK_COND
#define __GEN_ASK_COND

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <inttypes.h>

#include "../Shared/frameStructure.h"
#include "../Shared/generateFrame.h"
#include "../UE/radioConditions.h"

#define TI_FOR_RADIO_COND_ASK 10
#define CODING_FOR_RADIO_COND_ASK 2 // Described in documentation
#define CC_FOR_RADIO_COND_ASK 1     // Must be always the same
#define CHAR_SIZE 8
#define MESSAGE_SIZE_IN_BYTES 4
#define MASK_BYTE_1 0xFF000000
#define MASK_BYTE_2 0x00FF0000
#define MASK_BYTE_3 0x0000FF00
#define MASK_BYTE_4 0x000000FF

frame generateRadioCondAskFrame(int uid, int tuid, int n);

#endif // !__GEN_ASK_COND