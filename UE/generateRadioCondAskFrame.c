#include "generateRadioCondAskFrame.h"

frame generateRadioCondAskFrame(int uid, int tuid, int n)
{
    char *data = calloc(sizeof(char), MESSAGE_SIZE_IN_BYTES);
    uint32_t cond = simulateRadioConditions();
    uint32_t temp = (cond & MASK_BYTE_1) >> (CHAR_SIZE * 3); // shift for 1st byte
    data[0] = temp;
    temp = (cond & MASK_BYTE_2) >> (CHAR_SIZE * 2); // shift for 2nd byte
    data[1] = temp;
    temp = (cond & MASK_BYTE_3) >> CHAR_SIZE; // shift for 3rd byte
    data[2] = temp;
    temp = cond & MASK_BYTE_4; // no shift, last byte
    data[3] = temp;

    frame radioCondFrame = generateFrame(TI_FOR_RADIO_COND_ASK, n, uid, CODING_FOR_RADIO_COND_ASK, tuid, 0, 1, data, 0);
    bool isLogged = radioCondFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.UID > 0 ? true : false;
    if (isLogged)
    {
        radioCondFrame.dataFrame.frameControlElements.frameControlElementsLogged.CC = CC_FOR_RADIO_COND_ASK;
    }
    else
    {
        radioCondFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.CC = CC_FOR_RADIO_COND_ASK;
    }

    return radioCondFrame;
}