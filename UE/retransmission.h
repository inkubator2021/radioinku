#ifndef __RETRANSMISSION
#define __RETRANSMISSION

#include "../Shared/frameStructure.h"
#include "../Shared/generateFrame.h"

frame generateRetransmissionFrame(frame prevFrame);

#endif // !__RETRANSMISSION