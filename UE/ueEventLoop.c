#include "ueEventLoop.h"
#include "frameSerialization.h"
#include "epollFunctions.h"
#include "string_to_frame.h"
#include "frameStructure.h"
#include "fillLoginData.h"

int socket_create(int net_flag, int addr_flag)
{
  // create transport endpoint; return file descriptor
  int socket_fd = socket(net_flag, addr_flag, 0);
  // error
  if (socket_fd < 0)
  {
    perror("Error opening socket");
    return -1;
  }

  return socket_fd;
}

int socket_connect(int socket_fd, char *server, unsigned int port_number)
{
  if ((int) port_number < 0)
  {
    perror("Invalid port number");
    return -1;
  }
  // Server address settings
  struct sockaddr_in server_addr;
  // set zero-valued bytes
  bzero((char*) &server_addr, sizeof(server_addr));
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(port_number);

  // Convert IP to binary
  if (inet_pton(AF_INET, server, &server_addr.sin_addr) <= 0)
  {
    perror("Error server");
    return -1;
  }
  // Connect to server
  if (connect(socket_fd, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0)
  {
    if (errno != EINPROGRESS)
    {
      perror("Error on conecting");
      return -1;
    }
  }
  return 0;
}

int handle_epoll_events(int number_of_events, int socket_fd, int epoll_fd, struct epoll_event *events, int epoll_flag)
{
  uint16_t bufferSize;
  uint8_t TI = 0;
  char buff[MAXBUF];
  for (int n = 0; n < number_of_events; ++n)
  {
    if (events[n].events & EPOLLIN)
    {
      int fd = events[n].data.fd;
      int received_bytes = 0;
      printf("Client received EPOLLIN! \n");
      memset(buff, 0, MAXBUF);

      received_bytes = recv_frame(fd, buff);

      if (received_bytes > 0)
      {
        frame frame = string_to_frame(buff); //deserializacja
        frameInterpretation(frame);
      }
      else
      {
        printf("No data received on client!\n");
      }

      printf("Client ready to send next signal!\n");
      if (epoll_control(epoll_fd, socket_fd, EPOLLOUT, epoll_flag) < 0)
      {
        perror("Error on epoll_ctl");
        return -1;
      }
    }
    else if (events[n].events & EPOLLOUT)
    {
      int fd = events[n].data.fd;
      unsigned char* loginData = fillLoginData();

      frame askForRadioCondition;

      char *data = calloc(sizeof(char), 4);
      data[0] = 0x38;
      data[1] = 0xA7;
      data[2] = 0xB4;
      data[3] = 0x3C;//to use simulateRadioConditions here

      askForRadioCondition = generateFrame(10,0,0,0,0,0,0,data,0);

      //GENERATE PADDING START
      int length = strlen(data) * BITS_IN_BYTE;
      uint16_t dataSizeBytes = length / BITS_IN_BYTE;
      uint16_t frameData_size = calculate_dataFrame_size(length) / BITS_IN_BYTE;
      uint16_t padding_size = frameData_size - dataSizeBytes;

      askForRadioCondition.dataFrame.frameData.padding = calloc(sizeof(char), padding_size);
      //GENERATE PADDING END
      //this should be done in function, maybe as part generateFrame?

      uint16_t bufferSize;
      unsigned char *buff = frameIntoBuffer(askForRadioCondition, &bufferSize);

      printf("Data size %uB, Padding Size %dB\n", dataSizeBytes, padding_size);
      printf("Sending frame of size %uB to server %d\n", bufferSize, fd);

      int bytes_sent = send_frame(fd, buff, bufferSize);
      if (bytes_sent < 0)
      {
        printf("Error on sending frame");
      }

      if (epoll_control(epoll_fd, socket_fd, EPOLLIN, epoll_flag) < 0)
      {
        perror("Error on epoll_ctl");
        return -1;
      }
    }
    else
    {
      return -1;
    }
  }

  return 0;
}

int handle_connection(char *server, unsigned int port_number, unsigned int max_epoll_events, int net_flag,
                      int epoll_create_flag)
{
  struct epoll_event events[QUEUE_LIMIT];
  char buffer[MAXBUF];
  int ret = 0;

  int socket_fd = socket_create(net_flag, SOCK_STREAM);
  if (socket_fd < 0)
  {
    perror("Error on socket_create");
    return -1;
  }

  if (socket_connect(socket_fd, server, port_number) < 0)
  {
    perror("Error on socket_connect");
    return -1;
  }

  int epoll_fd = epoll_create1(epoll_create_flag);
  if (epoll_fd < 0)
  {
    perror("Error on epoll create1");
    return -1;
  }

  if (epoll_control(epoll_fd, socket_fd, EPOLLOUT, EPOLL_CTL_ADD) < 0)
  {
    perror("Error on epoll_ctl");
    return -1;
  }

  // Wait for data - main loop
  while (ret >= 0)
  {
    printf("\nCLIENT...\n");
    int number_of_events = epoll_wait(epoll_fd, events, QUEUE_LIMIT, -1);
    if (number_of_events < 0)
    {
      perror("Error on Wait Epoll");
      return -1;
    }
    ret = handle_epoll_events(number_of_events, socket_fd, epoll_fd, events, EPOLL_CTL_MOD);
  }
  return ret;
}
