#ifndef SIMULATE_RADIO_CONDITIONS_H
#define SIMULATE_RADIO_CONDITIONS_H

#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <time.h>
//#include <assert.h>
#include <stdlib.h>



uint32_t simulateRadioConditions();
int randomiseNumberOfBitsToChange(int random);
bool checkIfNumberIsInArray(int NumberToCheck, int *arrayToCompare, int arraySize);


#endif
