#ifndef CLIENT_H
#define CLIENT_H

#include <arpa/inet.h>
#include <errno.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <sys/epoll.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include "epollFunctions.h"

#define MAXBUF 1024
#define SERVER "127.0.0.1"
#define PORT 8080
#define QUEUE_LIMIT 5

int socket_create(int net_flag, int addr_flag);

int socket_connect(int socket_fd, char *server, unsigned int port_number);

int handle_connection(char *server, unsigned int port_number,
                      unsigned int max_epoll_events, int net_flag, int epoll_create_flag);

int handle_epoll_events(int number_of_events, int socket_fd, int epoll_fd, struct epoll_event *events, int epoll_flag);

#endif
