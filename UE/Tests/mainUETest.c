#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include "clientEpollTestSuite.h"
#include "fillLoginDataTestSuite.h"
#include "radioConditionsSuite.h"
#include "generateRadioCondAskSuite.h"
#include "retransmissionSuite.h"

int main()
{
    runTestsGenRadioAskFrame();
    fillLoginDataTests();
    runRetransmisssonTests();
    runTests();
    client_epoll_test_suite();

    return 0;
}

