#include "retransmissionSuite.h"

//-------------------------------------------------------------------------------------------------
// PURPOSE: Verify generateRetransmissionFrame for ask for radio condidion frame

// PREREQUISITIES:
// a) generate frame (name: condFrame) using generateRadioCondAskFrame function with n argument
//   (transmission number) == 0

// TRIGGER:
// 1) Call generateRetransmissionFrame with condFrame as an argument

// VERIFICATION
// 1) Returned frame's transmission number == 1
// 2) Returned frame's coding == 2 (defined value for this kind of frame)
// 3) Returned frame's ciphering == 1 (defined value for this kind of frame)
// 4) Returned frame's data string is equal to condFrame data string
//-------------------------------------------------------------------------------------------------
void testRetrasmission1()
{
    printf("%s:%d\n", __func__, __LINE__);
    frame condFrame = generateRadioCondAskFrame(0, 24, 0);
    frame ret = generateRetransmissionFrame(condFrame);
    assert(ret.dataFrame.frameHeader.N == 1);
    assert(condFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.C == CODING_FOR_RADIO_COND_ASK);
    assert(condFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.CC == CC_FOR_RADIO_COND_ASK);
    assert(!(strcmp(condFrame.dataFrame.frameData.data, ret.dataFrame.frameData.data)));
    printf(__func__);
    printf(" done\n\n");
}

//-------------------------------------------------------------------------------------------------
// PURPOSE: Verify generateRetransmissionFrame for ask for data frame, unlogged user

// PREREQUISITIES:
// a) generate frame (name: prev) using generateFrame function (ti = 14, transmission number = 0, uid = 0,
//   tuid > 0, c = 1, short data string
//
// TRIGGER:
// 1) Call generateRetransmissionFrame with prev as an argument

// VERIFICATION
// 1) Returned frame's transmission number == 1
// 2) Returned frame's coding is equal to prev frame coding
// 3) Returned frame's ciphering is not equal to prev frame coding
// 4) Returned frame's data string is equal to prev data string
//-------------------------------------------------------------------------------------------------
void testRetrasmission2()
{
    printf("%s:%d\n", __func__, __LINE__);
    frame prev = generateFrame(14, 0, 0, 1, 32, 0, 1, "tygrysek", 0);
    frame ret = generateRetransmissionFrame(prev);
    assert(ret.dataFrame.frameHeader.N = 1);
    assert(prev.dataFrame.frameControlElements.frameControlElementsUnlogged.C ==
           ret.dataFrame.frameControlElements.frameControlElementsUnlogged.C);
    assert(prev.dataFrame.frameControlElements.frameControlElementsUnlogged.CC !=
           ret.dataFrame.frameControlElements.frameControlElementsUnlogged.CC);
    assert(!(strcmp(prev.dataFrame.frameData.data, ret.dataFrame.frameData.data)));
    printf(__func__);
    printf(" done\n\n");
}

//-------------------------------------------------------------------------------------------------
// PURPOSE: Verify generateRetransmissionFrame for ask for data frame, logged user, short data
// (subheader A)

// PREREQUISITIES:
// a) generate frame (name: prev) using generateFrame function (ti = 14, transmission number = 1, uid = 3,
//   tuid = 0, c = 2, short data string
// b) set ciphering value to 1
//
// TRIGGER:
// 1) Call generateRetransmissionFrame with prev as an argument

// VERIFICATION
// 1) Returned frame's transmission number == 1
// 2) Returned frame's coding == 3 (prev coding + 1)
// 3) Returned frame's ciphering is not equal to prev frame coding (==2)
// 4) Returned frame's data string is equal to prev data string
//-------------------------------------------------------------------------------------------------
void testRetrasmission3()
{
    printf("%s:%d\n", __func__, __LINE__);
    frame prev = generateFrame(14, 1, 3, 2, 0, 0, 1, "tygrysek", 0);
    prev.dataFrame.frameControlElements.frameControlElementsLogged.CC = 1;
    frame ret = generateRetransmissionFrame(prev);
    assert(prev.dataFrame.frameHeader.N == ret.dataFrame.frameHeader.N - 1);
    assert(prev.dataFrame.frameControlElements.frameControlElementsLogged.C ==
           ret.dataFrame.frameControlElements.frameControlElementsLogged.C - 1);
    assert(ret.dataFrame.frameControlElements.frameControlElementsLogged.CC == 2);
    assert(!(strcmp(prev.dataFrame.frameData.data, ret.dataFrame.frameData.data)));
    printf(__func__);
    printf(" done\n\n");
}

//-------------------------------------------------------------------------------------------------
// PURPOSE: Verify generateRetransmissionFrame for ask for data frame, logged user, long data
// (subheader B)

// PREREQUISITIES:
// a) generate frame (name: prev) using generateFrame function (ti = 14, transmission number = 1,
// uid = 3, tuid = 0, c = 3, long data string
// b) set cipheting value to 2
//
// TRIGGER:
// 1) Call generateRetransmissionFrame with prev as an argument

// VERIFICATION
// 1) Returned frame's transmission number == 1
// 2) Returned frame's coding is equal to prev coding value (3)
// 3) Returned frame's ciphering is not equal to prev frame coding (==1)
// 4) Returned frame's data string is equal to prev data string
//-------------------------------------------------------------------------------------------------
void testRetrasmission4()
{
    printf("%s:%d\n", __func__, __LINE__);
    char *data =
        "Miaow then turn around and show you my bum eat plants, meow, and throw up because i ate plants but rub my "
        "belly hiss. Just going to dip my paw in your coffee and do a taste test - oh never mind i forgot i don't like "
        "coffee - you can have that back now shred all toilet paper and spread around the house.";
    frame prev = generateFrame(14, 1, 3, 3, 0, 0, 1, data, 0);
    prev.dataFrame.frameControlElements.frameControlElementsLogged.CC = 2;
    frame ret = generateRetransmissionFrame(prev);
    assert(prev.dataFrame.frameHeader.N == ret.dataFrame.frameHeader.N - 1);
    assert(prev.dataFrame.frameControlElements.frameControlElementsLogged.C ==
           ret.dataFrame.frameControlElements.frameControlElementsLogged.C);
    assert(ret.dataFrame.frameControlElements.frameControlElementsLogged.CC == 1);
    assert(!(strcmp(prev.dataFrame.frameData.data, ret.dataFrame.frameData.data)));
    printf(__func__);
    printf(" done\n\n");
}

void runRetransmisssonTests()
{
    testRetrasmission1();
    testRetrasmission2();
    testRetrasmission3();
    testRetrasmission4();
}