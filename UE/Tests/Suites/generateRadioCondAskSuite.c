#include "generateRadioCondAskSuite.h"

//---------------------------------------------------------------
// PURPOSE:Verify generate_radio_cond_ask_frame function for
// logged UI
//
//  PREREQUISITES:
//  a) Passing positive uid argument
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Returned frame's TI == 10
//  2) Returned frame's N is equal to N argument
//  3) Returned frame's data char* length == 4 (4 bytes of radio conditions data)
//  4) Returned frame's UID is equal to uid argument
//---------------------------------------------------------------
void testGenRadioAskFrame1()
{
    printf("%s:%d\n", __func__, __LINE__);
    frame ask = generateRadioCondAskFrame(3, 0, 0);
    assert(ask.dataFrame.frameHeader.TI == TI_FOR_RADIO_COND_ASK);
    assert(ask.dataFrame.frameHeader.N == 0);
    assert(strlen(ask.dataFrame.frameData.data) == MESSAGE_SIZE_IN_BYTES);
    assert(ask.dataFrame.frameControlElements.frameControlElementsLogged.UID == 3);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify generate_radio_cond_ask_frame function for
// unlogged UI
//
//  PREREQUISITES:
//  a) Passing positive tuid argument and 0 as uid agument
//
//  TRIGGER:
//  1) Call function
//
//  VERIFICATION:
//  1) Returned frame's TI == 10
//  2) Returned frame's N is equal to N argument
//  3) Returned frame's data char* length == 4 (4 bytes of radio conditions data)
//  4) Returned frame's UID == 0
//  5) Returned frame's TUID is equal to tuid argument
//---------------------------------------------------------------
void testGenRadioAskFrame2()
{
    printf("%s:%d\n", __func__, __LINE__);
    frame ask = generateRadioCondAskFrame(0, 23, 0);
    assert(ask.dataFrame.frameHeader.TI == TI_FOR_RADIO_COND_ASK);
    assert(ask.dataFrame.frameHeader.N == 0);
    assert(strlen(ask.dataFrame.frameData.data) == MESSAGE_SIZE_IN_BYTES);
    assert(ask.dataFrame.frameControlElements.frameControlElementsUnlogged.UID == 0);
    assert(ask.dataFrame.frameControlElements.frameControlElementsUnlogged.TUID == 23);
    printf(__func__);
    printf(" done\n\n");
}

void runTestsGenRadioAskFrame()
{
    testGenRadioAskFrame1();
    testGenRadioAskFrame2();
}