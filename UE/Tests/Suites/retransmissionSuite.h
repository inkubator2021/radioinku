#ifndef __RETRANSMISSION_TEST
#define __RETRANSMISSION_TEST

#include <assert.h>
#include <string.h>
#include "../../retransmission.h"
#include "../../../Shared/generateFrame.h"
#include "../../generateRadioCondAskFrame.h"

void testRetrasmission1();
void testRetrasmission2();
void testRetrasmission3();
void testRetrasmission4();
void runRetransmisssonTests();

#endif // !__RETRANSMISSION_TEST