#ifndef __CLIENT_EPOLL_TEST_SUITE
#define __CLIENT_EPOLL_TEST_SUITE

#include "epollFunctions.h"

#define SERVER "127.0.0.1"
#define PORT 8080
#define MAX_EPOLL_EVENTS 5

void test_1_socket_create();
void test_2_socket_create();
void test_1_socket_connect();
void test_2_socket_connect();
void test_3_socket_connect();
void test_4_socket_connect();
void test_1_epoll_create();
void test_2_epoll_create();
void test_1_handle_epoll_events();
void test_2_handle_epoll_events();
void test_3_handle_epoll_events();
void test_4_handle_epoll_events();
void test_5_handle_epoll_events();
void test_2_handle_connection();
void test_3_handle_connection();
void test_4_handle_connection();
void test_5_handle_connection();
void test_6_handle_connection();
void test_7_handle_connection();

void client_epoll_test_suite();

#endif // !__CLIENT_EPOLL_TEST_SUITE

