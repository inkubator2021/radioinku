#include "radioConditionsSuite.h"

//---------------------------------------------------------------
// PURPOSE:Verify testCheckIfNumberIsRepeated function
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) function checks if a number in array has been repeated
//  2) function returns true if number already is in an array
//  3) function returns false if number isn't in an array
//---------------------------------------------------------------

void testCheckIfNumberIsRepeated()
{
    int numberToCheck = 3;
    int arraySize = 5;
    int arrayToCompare1[arraySize];
    int arrayToCompare2[arraySize];
    arrayToCompare1[2] = 3;
    arrayToCompare2[2] = 10;
    bool testBoolTrue = checkIfNumberIsInArray(numberToCheck,arrayToCompare1,arraySize);
    bool testBoolFalse = checkIfNumberIsInArray(numberToCheck, arrayToCompare2, arraySize);
    assert(testBoolTrue == true);
    assert(testBoolFalse == false);
}

//---------------------------------------------------------------
// PURPOSE:Verify RandomiseNumberOfBitsToChange function
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) function returns values from 0 to 6 depending on percentage range of argument
//  2) function returns -1 if argument is out of range
//---------------------------------------------------------------

void testRandomiseNumberOfBitsToChange()
{
    assert(randomiseNumberOfBitsToChange(10) == 0);
    assert(randomiseNumberOfBitsToChange(30) == 1);
    assert(randomiseNumberOfBitsToChange(50) == 2);
    assert(randomiseNumberOfBitsToChange(70) == 3);
    assert(randomiseNumberOfBitsToChange(80) == 4);
    assert(randomiseNumberOfBitsToChange(90) == 5);
    assert(randomiseNumberOfBitsToChange(100) == 6);
    assert(randomiseNumberOfBitsToChange(101) == -1);   
}

//---------------------------------------------------------------
// PURPOSE:Verify simulateRadioConditions function
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) function returns values from 242748 to 4293375036 depending on 
//  number of bits to change and position of those bits
//---------------------------------------------------------------

void testSimulateRadioConditions()
{
    assert(simulateRadioConditions() <= 0b11111111111001111011010000111100);
    assert(simulateRadioConditions() >= 0b00000000000000111011010000111100);
}

void runTests()
{
    testCheckIfNumberIsRepeated();
    testRandomiseNumberOfBitsToChange();
    testSimulateRadioConditions();
}
