#include "fillLoginDataTestSuite.h"

//---------------------------------------------------------------
// PURPOSE: Verify fillLoginData function
//
//  PREQUISITES:
//  a) None
//
//  TRIGGER:
//  1) calling function
//
//  VERIFICATION:
//  1) function return non NULL pointer
//---------------------------------------------------------------

void fillLoginDataTest1()
{
  printf("%s:%d\n", __func__, __LINE__);
  unsigned char* ret = fillLoginData();

  assert(ret != NULL);
  free(ret);

  printf("Test %s passed\n\n",  __func__);
}

void fillLoginDataTests()
{
  fillLoginDataTest1();
}

