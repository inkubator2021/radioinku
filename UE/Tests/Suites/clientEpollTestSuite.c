#include "ueEventLoop.h"
#include "UETestWrappers.h"
#include "clientEpollTestSuite.h"

extern bool epoll_wait_in;
extern bool epoll_wait_out;
extern bool epoll_epollet;
extern bool mock_socket;
extern bool mock_epoll_ctl;
extern bool mock_connect;
extern bool mock_handle_epoll_events;

//---------------------------------------------------------------
// PURPOSE:Verify socket_create function with valid arguments
//
//  PREQUISITES:
//  a) Passing valid arguments to function
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns non negative value
//  2) Function doesn't return error code
//---------------------------------------------------------------
void test_1_socket_create()
{
    printf("%s:%d\n", __func__, __LINE__);
    int fd = socket_create(AF_INET, SOCK_STREAM);
    assert(fd >= 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify socket_create function with invalid arguments
//
//  PREQUISITES:
//  a) Passing 2 invalid arguments to function
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  2) Function returns error code
//---------------------------------------------------------------
void test_2_socket_create()
{
    printf("%s:%d\n", __func__, __LINE__);
    int fd = socket_create(-1, -1);
    assert(fd < 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify socket_connect function with valid arguments
//
//  PREQUISITES:
//  a) Create socket_fd with socket_create function
//  a) Passing 2 invalid arguments to function
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns 0
//  2) Function doesn't return error code
//---------------------------------------------------------------
void test_1_socket_connect()
{
    printf("%s:%d\n", __func__, __LINE__);
    int socket_fd = socket_create(AF_INET, SOCK_STREAM);
    int ret = socket_connect(socket_fd, SERVER, PORT);
    assert(ret == 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify socket_connect function with invalid socket_fd
//
//  PREQUISITES:
//  a) Set socket_fd to negative value
//  a) Set mock_connect to false
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  2) Function returns error code
//---------------------------------------------------------------
void test_2_socket_connect()
{
    printf("%s:%d\n", __func__, __LINE__);
    int socket_fd = -1;
    mock_connect = false;
    int ret = socket_connect(socket_fd, SERVER, PORT);
    assert(ret < 0);
    mock_connect = true;
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify socket_connect function with invalid server argument
//
//  PREQUISITES:
//  a) Create socket_fd with socket_create function
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  2) Function returns error code
//---------------------------------------------------------------
void test_3_socket_connect()
{
    printf("%s:%d\n", __func__, __LINE__);
    int socket_fd = socket_create(AF_INET, SOCK_STREAM);
    int ret = socket_connect(socket_fd, "server", PORT);
    assert(ret < 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify socket_connect function with invalid port argument
//
//  PREQUISITES:
//  a) Create socket_fd with socket_create function
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  2) Function returns error code
//---------------------------------------------------------------
void test_4_socket_connect()
{
    printf("%s:%d\n", __func__, __LINE__);
    int socket_fd = socket_create(AF_INET, SOCK_STREAM);
    assert(socket_connect(socket_fd, SERVER, -1) < 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify epoll_create function with valid argument
//
//  PREQUISITES:
//  a) Pass valid argument to function
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns non negative value
//  2) Function doesn't return error code
//---------------------------------------------------------------
void test_1_epoll_create()
{
    printf("%s:%d\n", __func__, __LINE__);
    assert(epoll_create(0) >= 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify epoll_create function with invalid argument
//
//  PREQUISITES:
//  a) Pass invalid (negative) argument to function
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  2) Function returns error code
//---------------------------------------------------------------
void test_2_epoll_create()
{
    printf("%s:%d\n", __func__, __LINE__);
    assert(epoll_create(-1) < 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_epoll_events function with invalid argument
// (number_of_events) when rest of arguments is valid
//
//  PREQUISITES:
//  a) Create epoll_event structure
//  b) Create socket_fd wotj spclet_create function
//  c) Connect with socket_connect function
//  d) Create epoll_fd
//  e) Set number_of_events to negative value
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns 0
//  2) Function doesn't return error code
//  3) No message from server (skipping the loop)
//---------------------------------------------------------------
void test_1_handle_epoll_events()
{
    printf("%s:%d\n", __func__, __LINE__);
    struct epoll_event events[QUEUE_LIMIT];
    int socket_fd = socket_create(AF_INET, SOCK_STREAM);
    socket_connect(socket_fd, SERVER, PORT);
    int epoll_fd = epoll_create1(0);
    assert(handle_epoll_events(-1, socket_fd, epoll_fd, events, -1) == 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_epoll_events function with EPOLLOUT
// event and faild epoll_ctl
//
//  PREQUISITES:
//  a) Create epoll_event structure
//  b) Create socket_fd wotj spclet_create function
//  c) Connect with socket_connect function
//  d) Create epoll_fd
//  e) Set epoll_wait_out to true and other epoll_wait flags to
//      false (if needed)
//  f) Set mock_epoll_ctl to true
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Receive confirmation of send message
//  2) Function returns negative value
//  3) Function returns error code
//---------------------------------------------------------------
void test_2_handle_epoll_events()
{
    printf("%s:%d\n", __func__, __LINE__);
    struct epoll_event events[QUEUE_LIMIT];
    int socket_fd = socket_create(AF_INET, SOCK_STREAM);
    int epoll_fd = epoll_create1(0);
    int number_of_events = epoll_wait(epoll_fd, events, QUEUE_LIMIT, -1);
    mock_epoll_ctl = true;
    assert(handle_epoll_events(number_of_events, socket_fd, epoll_fd, events, EPOLL_CTL_ADD) < 0);
    mock_epoll_ctl = false;
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_epoll_events function with EPOLLIN
// event and faild epoll_ctl
//
//  PREQUISITES:
//  a) Create epoll_event structure
//  b) Create socket_fd wotj spclet_create function
//  c) Connect with socket_connect function
//  d) Create epoll_fd
//  e) Set epoll_wait_in to true and other epoll_wait flags to
//      false (if needed)
//  f) Set mock_epoll_ctl to true
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Receive test message from server
//  2) Function returns negative value
//  3) Function returns error code
//---------------------------------------------------------------
void test_3_handle_epoll_events()
{
    printf("%s:%d\n", __func__, __LINE__);
    struct epoll_event events[QUEUE_LIMIT];
    int socket_fd = socket_create(AF_INET, SOCK_STREAM);
    int epoll_fd = epoll_create1(0);
    epoll_wait_out = false;
    epoll_wait_in = true;
    int number_of_events = epoll_wait(epoll_fd, events, QUEUE_LIMIT, -1);
    mock_epoll_ctl = true;
    assert(handle_epoll_events(number_of_events, socket_fd, epoll_fd, events, EPOLL_CTL_ADD) < 0);
    mock_epoll_ctl = false;
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_epoll_events function with EPOLLIN
// event and faild epoll_ctl
//
//  PREQUISITES:
//  a) Create epoll_event structure
//  b) Create socket_fd wotj spclet_create function
//  c) Connect with socket_connect function
//  d) Create epoll_fd
//  e) Set epoll_wait_in to true and other epoll_wait flags to
//      false (if needed)
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Receive test message from server
//  2) Function returns non negative value
//  3) Function doesn't return error code
//---------------------------------------------------------------
void test_4_handle_epoll_events()
{
    printf("%s:%d\n", __func__, __LINE__);
    struct epoll_event events[QUEUE_LIMIT];
    int socket_fd = socket_create(AF_INET, SOCK_STREAM);
    int epoll_fd = epoll_create1(0);
    int number_of_events = epoll_wait(epoll_fd, events, QUEUE_LIMIT, -1);
    assert(handle_epoll_events(number_of_events, socket_fd, epoll_fd, events, EPOLL_CTL_ADD) >= 0);

    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_epoll_events function with EPOLLOUT
// event and faild epoll_ctl
//
//  PREQUISITES:
//  a) Create epoll_event structure
//  b) Create socket_fd wotj spclet_create function
//  c) Connect with socket_connect function
//  d) Create epoll_fd
//  e) Set epoll_wait_out to true and other epoll_wait flags to
//      false (if needed)
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Receive confirmation of send message
//  2) Function returns non negative value
//  3) Function doesn't return error code
//---------------------------------------------------------------
void test_5_handle_epoll_events()
{
    printf("%s:%d\n", __func__, __LINE__);
    struct epoll_event events[QUEUE_LIMIT];
    epoll_wait_out = true;
    epoll_wait_in = false;
    int socket_fd = socket_create(AF_INET, SOCK_STREAM);
    int epoll_fd = epoll_create1(0);
    int number_of_events = epoll_wait(epoll_fd, events, QUEUE_LIMIT, -1);
    assert(handle_epoll_events(number_of_events, socket_fd, epoll_fd, events, EPOLL_CTL_ADD) >= 0);

    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_connection with wrong argument (server)
//
//  PREQUISITES:
//  a) Set server to invalid value
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  3) Function returns error code
//---------------------------------------------------------------
void test_1_handle_connection()
{
    printf("%s:%d\n", __func__, __LINE__);
    assert(handle_connection("server", PORT, MAX_EPOLL_EVENTS, AF_INET, 0) < 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_connection with wrong argument (port_number)
//
//  PREQUISITES:
//  a) Set port_number to invalid value
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  3) Function returns error code
//---------------------------------------------------------------
void test_2_handle_connection()
{
    printf("%s:%d\n", __func__, __LINE__);
    assert(handle_connection(SERVER, -1, MAX_EPOLL_EVENTS, AF_INET, 0) < 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_connection with wrong argument (net_flag)
//
//  PREQUISITES:
//  a) Set net_flag to invalid value
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  3) Function returns error code
//---------------------------------------------------------------
void test_3_handle_connection()
{
    printf("%s:%d\n", __func__, __LINE__);
    assert(handle_connection(SERVER, PORT, MAX_EPOLL_EVENTS, -1, 0) < 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_connection with wrong argument (epoll_create_flag)
//
//  PREQUISITES:
//  a) Set epoll_create_flag to invalid value
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  3) Function returns error code
//---------------------------------------------------------------
void test_4_handle_connection()
{
    printf("%s:%d\n", __func__, __LINE__);
    assert(handle_connection(SERVER, PORT, MAX_EPOLL_EVENTS, AF_INET, -1) < 0);
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_connection with epoll_wait returning negative value
//
//  PREQUISITES:
//  a) Set all epoll_wait flas to false
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  3) Function returns error code
//---------------------------------------------------------------
void test_5_handle_connection()
{
    printf("%s:%d\n", __func__, __LINE__);
    epoll_wait_out = false;
    epoll_wait_in = false;
    assert(handle_connection(SERVER, PORT, MAX_EPOLL_EVENTS, AF_INET, 0) < 0);
    epoll_wait_out = true;
    epoll_wait_in = false;
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_connection with epoll_ctl returning negative value
//
//  PREQUISITES:
//  a) Set mock_epoll_ctl to true
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  3) Function returns error code
//---------------------------------------------------------------
void test_6_handle_connection()
{
    printf("%s:%d\n", __func__, __LINE__);
    mock_epoll_ctl = true;
    assert(handle_connection(SERVER, PORT, MAX_EPOLL_EVENTS, AF_INET, 0) < 0);
    mock_epoll_ctl = false;
    printf(__func__);
    printf(" done\n\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify handle_connection with not supportet event type (EPOLLET)
//
//  PREQUISITES:
//  a) Set epoll_epollet to true, and other epoll_wait flags to false
//
//  TRIGGER:
//  1) Running function
//
//  VERIFICATION:
//  1) Function returns negative value
//  3) Function returns error code
//---------------------------------------------------------------
void test_7_handle_connection()
{
    printf("%s:%d\n", __func__, __LINE__);
    epoll_wait_out = false;
    epoll_wait_in = false;
    epoll_epollet = true;
    assert(handle_connection(SERVER, PORT, MAX_EPOLL_EVENTS, AF_INET, 0) < 0);
    printf(__func__);
    epoll_wait_out = true;
    epoll_wait_in = false;
    epoll_epollet = false;
    printf(" done\n\n");
}

void client_epoll_test_suite()
{
    test_1_socket_create();
    test_2_socket_create();
    test_1_socket_connect();
    test_2_socket_connect();
    test_3_socket_connect();
    test_4_socket_connect();
    test_1_handle_connection();
    test_2_handle_connection();
    test_3_handle_connection();
    test_4_handle_connection();
    test_5_handle_connection();
    test_6_handle_connection();
    test_7_handle_connection();
    test_1_epoll_create();
    test_2_epoll_create();
    test_1_handle_epoll_events();
    test_2_handle_epoll_events();
    test_3_handle_epoll_events();
    //test_4_handle_epoll_events();
    //test_5_handle_epoll_events();

    printf(__func__);
    printf(" done\n\n");
}
