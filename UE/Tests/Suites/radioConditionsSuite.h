#ifndef RADIO_CONDITIONS_SUITE
#define RADIO_CODNITIONS_SUITE

#include "radioConditions.h"
#include <assert.h>

void runTests();
void testCheckIfNumberIsRepeated();
void testRandomiseNumberOfBitsToChange();
void testSimulateRadioConditions();

#endif
