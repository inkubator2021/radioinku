#ifndef __GEN_ASK_COND_TESTS
#define __GEN_ASK_COND_TESTS

#include <assert.h>
#include <string.h>
#include "../../generateRadioCondAskFrame.h"

void testGenRadioAskFrame1();
void testGenRadioAskFrame2();
void runTestsGenRadioAskFrame();

#endif // !__GEN_ASK_COND_TESTS
