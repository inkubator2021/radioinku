#include <assert.h>
#include <stdlib.h>
#include <time.h>
#include <stdio.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <signal.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <errno.h>
#include <strings.h>
#include <unistd.h>
#include <stdbool.h>
#include "ueEventLoop.h"
#include "epollFunctions.h"

bool epoll_wait_out = true;
bool epoll_wait_in = false;
bool mock_socket = false;
bool mock_epoll_ctl = false;
bool mock_connect = true;
bool mock_handle_epoll_events = false;
bool epoll_epollet = false;

int __real_epoll_ctl(int epfd, int op, int fd, struct epoll_event *event);
int __wrap_epoll_ctl(int epfd, int op, int fd, struct epoll_event *event)
{
    if (mock_epoll_ctl == true)
    {
        return -1;
    }
    else
    {
        return __real_epoll_ctl(epfd, op, fd, event);
    }
}

int __wrap_epoll_wait(int epfd, struct epoll_event *events,
                      int maxevents, int timeout)
{
    if (epoll_wait_in == true)
    {
        events->events = EPOLLIN;
        maxevents = 1;
    }
    else if (epoll_wait_out == true)
    {
        events->events = EPOLLOUT;
        maxevents = 1;
    }
    else if (epoll_epollet == true)
    {
        events->events = EPOLLET;
        maxevents = 1;
    }
    else
    {
        maxevents = -1;
    }
    return maxevents;
}

int __real_socket(int domain, int type, int protocol);
int __wrap_socket(int domain, int type, int protocol)
{
    if (mock_socket == true)
    {
        return -1;
    }
    else
    {
        return __real_socket(domain, type, protocol);
    }
}

int __real_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen);
int __wrap_connect(int sockfd, const struct sockaddr *addr, socklen_t addrlen)
{
    if (mock_connect == true)
    {
        return 0;
    }
    else
    {
        return __real_connect(sockfd, addr, addrlen);
    }
}

int __wrap_send_msg(int fd)
{
    char buffer[MAXBUF];
    bzero(buffer, MAXBUF);
    int i = 0;

    char message[] = "Hello";

    while (i < strlen(message))
    {
        buffer[i++] = message[i];
    }

    send(fd, buffer, sizeof(buffer), 0);
    printf("Message sent from client\n");
}

int __wrap_receive(int fd)
{
    printf("Message received from server: TEST\n");
    return fd;
}
