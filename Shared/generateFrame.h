#ifndef GENERATE_FRAME_H
#define GENERATE_FRAME_H

#include "logger.h"
#include "frameStructure.h"

union frame generateFrame(int header_TI_value, int header_N_value,
                          int controle_UID_value, int controle_C_value, int controle_TUID_value,
                          int subheader_Num_value, int subheader_last,
                          char *data,
                          int HSinfo_ack
                         );
                        


int getRandInt(int max);
struct frameHeader createHeader(int TI_value, int N_value);
struct frameData createData(char *data);
struct frameHSinfo createHSinfo(int UID_value,int NUM_value,int ack);
struct frameSubheaderA createSubheaderA(int Num_value,int last_value,int LENGTH_value);
struct frameSubheaderB createSubheaderB(int Num_value,int last_value,int LENGTH_value);
struct frameControlElementsUnlogged createControlElementsUnlogged(int UID_value, int C_value, int TUID_value);
struct frameControlElementsLogged createControlElementsLogged(int UID_value,int C_value);


#endif
