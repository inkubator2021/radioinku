#include "epollFunctions.h"

int recv_frame(int fd, unsigned char *buff)
{
  int ret = recv(fd, buff, MAXBUF - 1, 0);
  if (ret < 0)
  {
    if (errno == EAGAIN || errno == EWOULDBLOCK)
    {
      printf("Skip reading\n");
    }
    else
    {
      printf("Close socket!\n");
      close(fd);
    }
  }
  else if (ret == 0)
  {
    printf("Server read complete!\n");
    close(fd);
  }
  else
  {
    printf("Received %d bytes from socket\n", ret);
  }
  return ret;
}

int send_frame(int fd, unsigned char *buff, unsigned int bufferSize)
{
  int bytes_sent = 0;

  if (buff == NULL)
  {
    bytes_sent = -1;
  }
  else if(fd < 0)
  {
    bytes_sent = -1;
    free(buff);
  }
  else
  {
    bytes_sent = send(fd, buff, bufferSize, 0);
  }
  return bytes_sent;
}

int set_socket_to_non_block(int socket_fd, int sock_block_flag)
{
  int block_flag = fcntl(socket_fd, sock_block_flag, fcntl(socket_fd, F_GETFL, 0) | O_NONBLOCK);

  if (block_flag == -1)
  {
    perror("Error on fcntl O_NONBLOCK");
    return -1;
  }

  return 1;
}

int epoll_create(int epoll_create_flag)
{
  int epoll_fd = epoll_create1(epoll_create_flag);
  if (epoll_fd < 0)
  {
    perror("Error epoll create1");
    return -1;
  }

  return epoll_fd;
}

int epoll_control(int epoll_fd, int socket_fd, int state, int epoll_flag)
{
//MONITOR FOR THE ARRIVAL OF NEW DATA TO THE NEW SOCKET BUFFER - EPOLLIN FLAG
  struct epoll_event main_event;
  int ret_val = 1;

  main_event.events = state | EPOLLET;
  main_event.data.fd = socket_fd;

//ADDING FILE DESCRIPTORS OUR PROCESS WANT TO MONITOR TO EPOLL INSTANCE - CREATING EPOLL SET OR EPOLL INTEREST LIST
  if (epoll_ctl(epoll_fd, epoll_flag, socket_fd, &main_event) < 0)
  {
    perror("Error on epoll_ctl create_epoll_event");
    ret_val = -1;
  }

  return ret_val;
}
