#include "frameSerialization.h"

unsigned char* frameHeaderSerial(unsigned char *buffer, struct frameHeader *arg)
{
  buffer[0] |= arg->TI << 3;
  buffer[0] |= arg->N << 1;
  buffer[0] |= arg->R;

  return buffer + sizeof(char);
}

unsigned char* frameControlElementsLoggedSerial(unsigned char *buffer, struct frameControlElementsLogged *arg)
{
  buffer[0] |= arg->UID << 5;
  buffer[0] |= arg->C << 3;
  buffer[0] |= arg->CC << 1;
  buffer[0] |= arg->R;
  buffer[1] = arg->RESERVED;

  return buffer + sizeof(struct frameControlElementsLogged);
}

unsigned char* frameControlElementsUnloggedSerial(unsigned char *buffer, struct frameControlElementsUnlogged *arg)
{
  buffer[0] |= arg->UID << 5;
  buffer[0] |= arg->C << 3;
  buffer[0] |= arg->CC << 1;
  buffer[0] |= arg->R;
  buffer[1] = arg->TUID;
  buffer[2] = arg->RESERVED;

  return buffer + sizeof(struct frameControlElementsUnlogged);
}

unsigned char* frameSubheaderASerial(unsigned char *buffer, struct frameSubheaderA *arg)
{
  buffer[0] |= arg->F << 7;
  buffer[0] |= arg->Num << 1;
  buffer[0] |= arg->L;
  buffer[1] = arg->LENGTH;

  return buffer + sizeof(struct frameSubheaderA);
}

unsigned char* frameSubheaderBSerial(unsigned char *buffer, struct frameSubheaderB *arg) //size 3 bytes
{
  buffer[0] |= arg->F << 7;
  buffer[0] |= arg->Num << 1;
  buffer[0] |= arg->L;
  buffer[1] |= (arg->LENGTH) >> 8;
  buffer[2] = arg->LENGTH;

  return buffer + sizeof(struct frameSubheaderB);
}

unsigned char* frameData(unsigned char *buffer, struct frameData *arg, uint16_t length)
{
  if (arg->data == NULL || arg->padding == NULL)
  {
    perror("data NULL");
    return buffer;
  }

  uint16_t dataSizeBytes = length / BITS_IN_BYTE; //+ (length % 8 != 0); //data size in bytes
  uint16_t frameData_size = calculate_dataFrame_size(length) / BITS_IN_BYTE;

  printf("Data frame size %d\n", frameData_size);
  uint8_t padding_size = frameData_size - dataSizeBytes;

  memcpy(buffer, arg->data, dataSizeBytes);
  memcpy(buffer+dataSizeBytes, arg->padding, padding_size);

  free(arg->data);
  free(arg->padding);

  return buffer + frameData_size;
}

unsigned char* frameHSinfoSerial(unsigned char *buffer, struct frameHSinfo *arg) //size 3 bytes
{
  buffer[0] = arg->UID;
  buffer[1] |= arg->NUM << 2;
  buffer[1] |= arg->A << 1;
  buffer[1] |= arg->R;

  return buffer + sizeof(struct frameHSinfo);
}

//frame serialize - generic
unsigned char* frameIntoBuffer(frame frame, uint16_t *bufferSize)
{
  unsigned char *buffer, *ptr;
  bool isLogged = false;
  bool isSubheaderB = false;

  uint8_t ti = frame.dataFrame.frameHeader.TI;
  uint16_t dataSizeBytes = 0;
  uint16_t length = 0;
  buffer = malloc(sizeof(char) * MAX_BUFF);

  if (buffer != NULL)
  {
    memset(buffer, 0, sizeof(char) * MAX_BUFF);
  }
  ptr = buffer;

  ptr = frameHeaderSerial(ptr, &frame.dataFrame.frameHeader);

  if (ti != 1)
  {
    isLogged = frame.dataFrame.frameControlElements.frameControlElementsUnlogged.UID > 0 ? true : false;
    isSubheaderB = frame.dataFrame.frameSubheader.frameSubheaderA.F > 0 ? true : false;

    if (isLogged)
    {
      ptr = frameControlElementsLoggedSerial(ptr, &frame.dataFrame.frameControlElements.frameControlElementsLogged);
    }
    else
    {
      ptr = frameControlElementsUnloggedSerial(ptr,
                                                &frame.dataFrame.frameControlElements.frameControlElementsUnlogged);
    }

    if (isSubheaderB)
    {
      ptr = frameSubheaderBSerial(ptr, &frame.dataFrame.frameSubheader.frameSubheaderB);
      length = frame.dataFrame.frameSubheader.frameSubheaderB.LENGTH;
    }
    else
    {
      ptr = frameSubheaderASerial(ptr, &frame.dataFrame.frameSubheader.frameSubheaderA);
      length = frame.dataFrame.frameSubheader.frameSubheaderA.LENGTH;
    }

    ptr = frameData(ptr, &frame.dataFrame.frameData, length);
  }
  else
  {
    printf("Serialize Hand-Shake Frame\n");
    ptr = frameHSinfoSerial(ptr, &frame.handshakeFrame.frameHSinfo);
  }


  *bufferSize = ptr - buffer;

  printf("frameIntoBuffer, size %d\n", *bufferSize);

  return buffer;
}
