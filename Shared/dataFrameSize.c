#include "dataFrameSize.h"

uint16_t calculate_dataFrame_size(uint16_t size_r)
{
  uint16_t padding_size;
  uint8_t n;
  uint16_t size[MAX_N] = {16, 48, 144, 432, 1296, 3888};

  for (n = 0; n < MAX_N; n++)
  {
    if (size[n] >= size_r)
    {
      break;
    }
    else if(n == MAX_N - 1)
    {
      return size[n];
    }
  }

  padding_size = size[n] - size_r;

  if (padding_size <= 0.4 * size[n] || n == 0)
  {
    return size[n];
  }
  else
  {
    return size[n-1];
  }
}
