#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "logger.h"

#define STATUS_MESSAGE_LENGTH 20
#define PATH_MAX 50


//create time structure 
struct tm getTime(void){
    struct tm timeStructure; 
    time_t current_time;
    current_time = time(NULL);
    timeStructure = *(localtime(&current_time));
    return timeStructure;
}

//create directory -1 on error ,0 on sucess
//checking if logs directory exists if not then creating it in current directory
int createDirectory(void){

    struct stat sb;
    if (stat("./logs",&sb) == -1){
        return mkdir("./logs",0700); //zero on success
    }
    return 0;
}

//create time stamp DD/MM/YYYY ,HH:MM::SS like this >> 26-April-2021---17:26:25
//returns allocated memory space free it after use 
char* getTimeStamp(void){
    struct tm timeStructure = getTime();
    char *timeStampString = malloc(LOG_TIMESTAMP_SIZE);
    
    strftime(timeStampString,LOG_TIMESTAMP_SIZE," - %d-%B-%Y---%H:%M:%S\n",&timeStructure);
    return timeStampString;
}

//concat two strings givas as arguments 
//return allocated memory space free it after use
char *concatMessageString(char* stringLeft, char* stringRight){

    size_t size = strlen(stringLeft)+strlen(stringRight);
    char *buffer = malloc(size);
    
    strcpy(buffer,stringLeft);
    strcat(buffer,stringRight);
   
    return buffer;
}

//takes int ID and create name and directory for file name 
//return allocated memory space free it after use
char *getFileName(int ID){
    char logFileName[12];
    sprintf(logFileName, "%d", ID);
    char* logName =  concatMessageString(logFileName,".log");
    char* logNameDirectory = concatMessageString("./logs/",logName);
    free(logName);
    return logNameDirectory;
}

//test
//Pass char* massage and int ID given by loggerInit 
int writeToFile(char* message,int ID){

    char* logFileName = getFileName(ID);
    char* timeStamp = getTimeStamp();
    char* fullMessage = concatMessageString(message,timeStamp);
    free(timeStamp);
    
    FILE *loggerInstance = fopen(logFileName,"a");
    if(loggerInstance != NULL){
        fprintf(loggerInstance,"%s",fullMessage);
        fclose(loggerInstance);
        free(logFileName);
        return 0;
    }
    free(logFileName);
    return -1;

}

int getID(void){
    srand(time(NULL));
    int randLogId = rand();
    return randLogId;
}

//example: int loggerInst = loggerInit("client");
//use only once at the begining of main file
int loggerInit(char *side){
    
    int logId = getID();
    createDirectory();
    writeToFile("Init log: ",logId);
    getSide(side);          
    getLoggerInstance(logId);
    return logId;
}

char* getSide(char *side){
    static char *loggerSide = "";
    
    if (strcmp(loggerSide,"") == 0)
    {
        loggerSide = side;
    }
    
    return loggerSide;
}

int getLoggerInstance(int passedId){
    static int id = 0;
    if(0 == id){
        id = passedId;
    }
    return id;
}

//flag WARNING_LOG, ERROR_LOG, NORMAL_LOG
//func fill with --func--
//example: int result = logger(NORMAL_LOG,--func--,"some logs");
//USE LOGGER_NORMAL(),LOGGER_WARNING(),LOGGER_ERROR() after init
int logger(char flag, const char *func, char *message){
    
    int id = getLoggerInstance(0);
    char *side = getSide("");
    char *fullMessage;
    char flagResult[STATUS_MESSAGE_LENGTH]; 
    
    switch (flag){
    case WARNING_LOG:
        strcpy(flagResult,"[Warning]-");
        break;
    
    case ERROR_LOG:
        strcpy(flagResult,"[Error]-  ");
        break;

    case NORMAL_LOG:
        strcpy(flagResult,"[Normal]- ");
        break;    
    default:
        return -1;
    }
    
    fullMessage = concatMessageString(side,flagResult);
    fullMessage = concatMessageString(fullMessage,func);
    fullMessage = concatMessageString(fullMessage," - ");
    fullMessage = concatMessageString(fullMessage,message);
    writeToFile(fullMessage,id);
    free(fullMessage);
    return id;

}
