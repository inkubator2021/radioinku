#include "ceasarCipher.h"

// remember to free memory after using this function
char *ceasarCiphering(char *message)
{
    int length = strlen(message);
    int numberOfCharacters = MAX_CHARACTER_VALUE - MIN_CHARACTER_VALUE + 1;

    int evenLength = length / 2;

    int oddLength = length % 2 == 0 ? evenLength : evenLength + 1;

    char *oddPart = calloc(oddLength, sizeof(char));
    char *evenPart = calloc(evenLength, sizeof(char));
    char *newMessage = calloc(length, sizeof(char));

    for (int i = 0; i < evenLength * 2; i += 2)
    {
        int temp = message[i] + LEFT_SHIFT;
        oddPart[i / 2] = temp <= MAX_CHARACTER_VALUE ? temp : temp - numberOfCharacters;
        temp = message[i + 1] - RIGHT_SHIFT;
        evenPart[i / 2] = temp >= MIN_CHARACTER_VALUE ? temp : temp + numberOfCharacters;
    }

    if (evenLength < oddLength)
    {
        int temp = message[length - 1] + LEFT_SHIFT;
        oddPart[oddLength - 1] = temp <= MAX_CHARACTER_VALUE ? temp : temp - numberOfCharacters;
    }

    memcpy(newMessage, oddPart, oddLength * sizeof(char));
    strcat(newMessage, evenPart);

    free(oddPart);
    free(evenPart);
    return newMessage;
}

// remember to free memory after using this function
char *ceasarDeciphering(char *message)
{
    int length = strlen(message);
    int numberOfCharacters = MAX_CHARACTER_VALUE - MIN_CHARACTER_VALUE + 1;

    int evenLength = length / 2;

    int oddLength = length % 2 == 0 ? evenLength : evenLength + 1;

    char *oddPart = calloc(oddLength, sizeof(char));
    char *evenPart = calloc(evenLength, sizeof(char));
    char *newMessage = calloc(length, sizeof(char));

    for (int i = 0; i < evenLength; i++)
    {
        int temp = message[i] - LEFT_SHIFT;
        oddPart[i] = temp >= MIN_CHARACTER_VALUE ? temp : temp + numberOfCharacters;
        temp = message[oddLength + i] + RIGHT_SHIFT;
        evenPart[i] = temp <= MAX_CHARACTER_VALUE ? temp : temp - numberOfCharacters;
    }

    if (evenLength < oddLength)
    {
        int temp = message[oddLength - 1] - LEFT_SHIFT;
        oddPart[oddLength - 1] = temp >= MIN_CHARACTER_VALUE ? temp : temp + numberOfCharacters;
    }

    for (int i = 0; i < evenLength * 2; i += 2)
    {
        newMessage[i] = oddPart[i / 2];
        newMessage[i + 1] = evenPart[i / 2];
    }
    if (evenLength < oddLength)
    {
        newMessage[length - 1] = oddPart[oddLength - 1];
    }

    free(oddPart);
    free(evenPart);
    return newMessage;
}