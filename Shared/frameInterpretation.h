#ifndef FRAME_INTERPRETATION_H
#define FRAME_INTERPRETATION_H

#include "../Shared/frameStructure.h"

enum action{loginError = -5, reservedBitsError = -4, retransmissionNumberError = -3,
typeIndicatorError = -2, typeIndicatorOutOfRange = -1, startLogingIn = 2,
startLogingOut = 3, startReserved = 4, startAFRC = 5, startIARC = 6, 
startDataTransmission = 7, startTest = 8, startHandshakeProcedure = 9,
correctHandshakeConfirmation = 10, start1stRetransmission = 11, 
start2ndRetransmission = 12, start3rdRetransmission = 13};

int frameInterpretation(frame frame);
int handshakeFrameInterpretation(frame frame);
int chooseFrameType(frame frame);

#endif