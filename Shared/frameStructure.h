#ifndef FRAME_STRUCTURE
#define FRAME_STRUCTURE

#include <stdio.h>
#include <stdint.h>

struct frameHeader
{
    uint8_t TI : 5; //Type Indicator
    uint8_t N : 2;  //Transmission number
    uint8_t R : 1;  //Reserved
};

struct frameControlElementsLogged
{
    uint8_t UID : 3;  //User ID
    uint8_t C : 2;    //Coding
    uint8_t CC : 2;   //Ciphering
    uint8_t R : 1;    //Reserved
    uint8_t RESERVED; //Reserved
};

struct frameControlElementsUnlogged
{
    uint8_t UID : 3;  //User ID
    uint8_t C : 2;    //Coding
    uint8_t CC : 2;   //Ciphering
    uint8_t R : 1;    //Reserved
    uint8_t TUID;     //Temporary User Id
    uint8_t RESERVED; //Reserved
};

union frameControlElements
{
    struct frameControlElementsLogged frameControlElementsLogged;
    struct frameControlElementsUnlogged frameControlElementsUnlogged;
};

struct frameSubheaderA
{
    uint8_t F : 1;   //Format [A/B]
    uint8_t Num : 6; //Data block number
    uint8_t L : 1;   //Last [0 - no / 1 - yes]
    uint8_t LENGTH;
};

struct __attribute__((__packed__)) frameSubheaderB
{
    uint8_t F : 1;   //Format [A/B]
    uint8_t Num : 6; //Data block number
    uint8_t L : 1;   //Last [0 - no / 1 - yes]
    uint16_t LENGTH;
};

union frameSubheader
{
    struct frameSubheaderA frameSubheaderA;
    struct frameSubheaderB frameSubheaderB;
};

struct frameData
{
    uint8_t *data;
    uint8_t *padding;
};

struct frameHSinfo
{
    uint8_t UID;     //User ID
    uint8_t NUM : 6; // Transmission number
    uint8_t A : 1;   // Ack/Nack
    uint8_t R : 1;   //Reserved
};

struct dataFrame
{
    struct frameHeader frameHeader;
    union frameControlElements frameControlElements;
    union frameSubheader frameSubheader;
    struct frameData frameData;
};

struct handshakeFrame
{
    struct frameHeader frameHeader;
    struct frameHSinfo frameHSinfo;
};

typedef union frame
{
    struct dataFrame dataFrame;
    struct handshakeFrame handshakeFrame;
} frame;

#endif
