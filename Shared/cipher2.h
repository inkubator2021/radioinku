#ifndef CIPHER2
#define CIPHER2

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

char *cipher2(char *message);
char *concat(uint8_t firstNumber, uint8_t secondNumber, uint8_t thirdNumber);

#endif