#include "string_to_frame.h"

// REMEMBER TO FREE MEMORY FOR DATA AND PADDING (IN CASE OF DATAFRAME)
frame string_to_frame(char *frame)
{
    union frame ret_frame;
    int counter = 0;
    int data_length = 0;
    uint16_t frame_size = 0;
    uint16_t padding_length = 0;
    char *data;
    // Header
    struct frameHeader header;
    header.TI = (frame[counter] & 0b11111000) >> 3;
    header.N = (frame[counter] & 0b00000110) >> 1;
    header.R = frame[counter] & 0b00000001;

    counter++;

    if (header.TI == 1) //Handshake
    {
        struct frameHSinfo hs_info;
        hs_info.UID = frame[counter]; //User ID
        counter++;
        hs_info.NUM = (frame[counter] & 0b11111100) >> 2; // Transmission number
        hs_info.A = (frame[counter] & 0b00000010) >> 1;   // Ack/Nack
        hs_info.R = frame[counter] & 0b00000001;          //Reserved

        struct handshakeFrame hs_frame = {header, hs_info};
        ret_frame.handshakeFrame = hs_frame;
        return ret_frame;
    }
    else
    {
        ret_frame.dataFrame.frameHeader = header;
        // Control Elements
        union frameControlElements CE;

        // First byte - always the same
        uint8_t UID = (frame[counter] & 0b11100000) >> 5;
        uint8_t C = (frame[counter] & 0b00011000) >> 3;
        uint8_t CC = (frame[counter] & 0b00000110) >> 1;
        uint8_t R_CE = frame[counter] & 0b00000001;
        counter++;

        if (UID == 0)
        {
            //Second and third byte - login procedure - login or asking for radio conditions dor unlogged user
            uint8_t TUID = frame[counter];
            counter++;
            uint8_t RESERVED_CE_U = frame[counter];
            counter++;

            struct frameControlElementsUnlogged CE_U = {UID, C, CC, R_CE, TUID, RESERVED_CE_U};
            CE.frameControlElementsUnlogged = CE_U;
            ret_frame.dataFrame.frameControlElements.frameControlElementsUnlogged = CE_U;
        }
        else
        {
            //Second byte - standard, only RESERVED
            uint8_t RESERVED_CE = frame[counter];
            counter++;

            struct frameControlElementsLogged CE_L = {UID, C, CC, R_CE, RESERVED_CE};
            CE.frameControlElementsLogged = CE_L;
            ret_frame.dataFrame.frameControlElements.frameControlElementsLogged = CE_L;
        }

        uint8_t F = (frame[counter] & 0b10000000) >> 7;
        uint8_t NUM_SUB = (frame[counter] & 0b01111110) >> 1;
        uint8_t L_SUB = frame[counter] & 0b00000001;
        counter++;
        if (F == 0) // subheader A
        {
            uint8_t LENGTH_A = frame[counter];
            counter++;
            data_length = LENGTH_A;
            struct frameSubheaderA SUB_A = {F, NUM_SUB, L_SUB, LENGTH_A};
            ret_frame.dataFrame.frameSubheader.frameSubheaderA = SUB_A;
        }
        else
        {
            uint16_t LENGHT_B = (uint16_t)frame[counter] << 8;
            counter++;
            LENGHT_B = LENGHT_B | (uint16_t)frame[counter];
            counter++;
            data_length = LENGHT_B;
            struct frameSubheaderB SUB_B = {F, NUM_SUB, L_SUB, LENGHT_B};
            ret_frame.dataFrame.frameSubheader.frameSubheaderB = SUB_B;
        }
        frame_size = calculate_dataFrame_size(data_length);
        padding_length = (frame_size - data_length);
        uint8_t *tempData = calloc(sizeof(char), data_length / BITS_IN_BYTE);
        ret_frame.dataFrame.frameData.data = calloc(sizeof(char),data_length/BITS_IN_BYTE);
        for (int i = 0; i < data_length / BITS_IN_BYTE; i++)
        {
            tempData[i] = frame[counter++];
        }
        memcpy(ret_frame.dataFrame.frameData.data, tempData, data_length / BITS_IN_BYTE);
        free(tempData);
        uint8_t *tempPad = calloc(sizeof(char),padding_length / BITS_IN_BYTE);
        ret_frame.dataFrame.frameData.padding = calloc(sizeof(char), padding_length / BITS_IN_BYTE);
        for (int i = 0; i < padding_length / BITS_IN_BYTE; i++)
        {
            tempPad[i] = frame[counter++];
        }
        memcpy(ret_frame.dataFrame.frameData.padding, tempPad, padding_length / BITS_IN_BYTE);
        free(tempPad);

        return ret_frame;
    }
}
