#include "cipher2.h"
#define FirstCharInAsciiPosition 32
#define NumberOfTables 4
#define NumberOfRows 5
#define NumberOfColumns 5
#define NumberOfElementsInTable 25
#define OutputLengthMultiplier 3


        // {{' ', '!', '"', '#', '$'},
        // {'%', '&', '\'', '(', ')'},
        // {'*', '+', ',', '-', '.'},
        // {'/', '0', '1', '2', '3'},
        // {'4', '5', '6', '7', '8'}},

        // {{'9', ':', ';', '<', '='},
        // {'>', '?', '@', 'A', 'B'},
        // {'C', 'D', 'E', 'F', 'G'},
        // {'H', 'I', 'J', 'K', 'L'},
        // {'M', 'N', 'O', 'P', 'Q'}},

        // {{'R', 'S', 'T', 'U', 'V'},
        // {'W', 'X', 'Y', 'Z', '['},
        // {'\\', ']', '^', '_', '`'},
        // {'a', 'b', 'c', 'd', 'e'},
        // {'f', 'g', 'h', 'i', 'j'}},

        // {{'k', 'l', 'm', 'n', 'o'},
        // {'p', 'q', 'r', 's', 't'},
        // {'u', 'v', 'w', 'x', 'y'},
        // {'z', '{', '|', '}', '~'},
        // {'\0', '\0', '\0', '\0', '\0'}}

char* cipher2(char *message)
{
    int length = strlen(message);
    char *newMessage = (char *)calloc(length * OutputLengthMultiplier, (length*sizeof(char))*OutputLengthMultiplier);
    
    for (int i=0; i < length; i++)
    {
        uint8_t j = (message[i]-FirstCharInAsciiPosition)/NumberOfElementsInTable;
        uint8_t k = ((message[i]-FirstCharInAsciiPosition) % NumberOfElementsInTable)/NumberOfRows;
        uint8_t l = ((message[i] - FirstCharInAsciiPosition) % NumberOfElementsInTable) % NumberOfColumns;
        strcat(newMessage, concat(j+1, k+1, l+1));    
    }
    return newMessage;
}

char* concat(uint8_t firstNumber, uint8_t secondNumber, uint8_t thirdNumber)
{
    char *string1 = (char *)calloc(OutputLengthMultiplier, OutputLengthMultiplier * sizeof(char));
    char string2[2];
    sprintf(string1, "%d", firstNumber);
    sprintf(string2, "%d", secondNumber);
    strcat(string1, string2);
    sprintf(string2, "%d", thirdNumber);
    strcat(string1, string2);
    return string1;
}
