#ifndef __FRAME_SERIALIZATION
#define __FRAME_SERIALIZATION

#include <stdlib.h>
#include <stdbool.h>

#include "logger.h"
#include "epollFunctions.h"
#include "frameStructure.h"
#include "generateFrame.h"
#include "dataFrameSize.h"

#define MAX_BUFF 500
#define BITS_IN_BYTE 8

unsigned char* frameHeaderSerial(unsigned char *buffer, struct frameHeader *header);

unsigned char* frameControlElementsLoggedSerial(unsigned char *buffer, struct frameControlElementsLogged *arg);

unsigned char* frameControlElementsUnloggedSerial(unsigned char *buffer, struct frameControlElementsUnlogged *arg);

unsigned char* frameSubheaderASerial(unsigned char *buffer, struct frameSubheaderA *arg);

unsigned char* frameSubheaderBSerial(unsigned char *buffer, struct frameSubheaderB *arg);

unsigned char* frameData(unsigned char *buffer, struct frameData *arg, uint16_t size);

unsigned char* frameHSinfoSerial(unsigned char *buffer, struct frameHSinfo *arg);

unsigned char* frameIntoBuffer(frame frame, uint16_t *bufferSize);

#endif
