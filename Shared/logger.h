#ifndef LOGGER_H
#define LOGGER_H
#define LOG_TIMESTAMP_SIZE 30
#define NORMAL_LOG 'n'
#define WARNING_LOG 'w'
#define ERROR_LOG 'e'

#define LOGGER_NORMAL(message) logger(NORMAL_LOG,__func__,message)
#define LOGGER_WARNING(message) logger(WARNING_LOG,__func__,message)
#define LOGGER_ERROR(message) logger(ERROR_LOG,__func__,message)


int loggerInit(char* side);
int logger(char flag, const char *func, char *message);

//function for testing purpose
struct tm getTime();
int createDirectory();
char* getTimeStamp();
char *concatMessageString(char* string1, char* string2);
char *getFileName(int ID);
int writeToFile(char* message,int ID);
int getID();
char* getSide(char *side);
int getLoggerInstance();

#endif