#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdint.h>
#include <string.h>
#include "generateFrame.h"
#include <limits.h>

#define SUBHEADER_A_MAX_VALUE 255

struct frameHeader createHeader(int TI_value, int N_value)
{
  //logger('n',__func__,"creating Header");
  struct frameHeader Header;
  Header.TI = TI_value;
  Header.N = N_value;
  Header.R = 0;

  return Header;
}

int getRandInt(int max)
{
  //logger('n',__func__,"geting rand int");
  srand(time(NULL));
  return rand() % max + 1;
}

//UID_value pass 0 on first transmission, on response pass generated UID 1-7
//C-value : coding value 0-3 values
//pass 0 to generate random tuid or pass TUID 1-255
struct frameControlElementsUnlogged createControlElementsUnlogged(int UID_value, int C_value, int TUID_value)
{
  struct frameControlElementsUnlogged ControlElementsUnlogged;

  ControlElementsUnlogged.UID = UID_value;
  ControlElementsUnlogged.C = C_value;
  ControlElementsUnlogged.CC = getRandInt(2);
  ControlElementsUnlogged.R = 0;
  ControlElementsUnlogged.RESERVED = 0;
  if (TUID_value == 0)
  {
    ControlElementsUnlogged.TUID = getRandInt(255);
  }
  else
  {
    ControlElementsUnlogged.TUID = TUID_value;
  }

  return ControlElementsUnlogged;
}

//UID_value pass 0 on first transmission, on response pass generated UID 1-7
//C-value : coding value 1-3 values
struct frameControlElementsLogged createControlElementsLogged(int UID_value, int C_value)
{
  //logger(NORMAL_LOG,__func__,"creating control element for logged user");
  struct frameControlElementsLogged ControlElementsLogged;
  ControlElementsLogged.UID = UID_value;
  ControlElementsLogged.C = C_value;
  ControlElementsLogged.CC = getRandInt(2);
  ControlElementsLogged.R = 0;
  ControlElementsLogged.RESERVED = 0;
  return ControlElementsLogged;
}

//Subheader A has Foramat value = 0
//Num_value data block number 0-127 
//last_flag bit that inform if frame contains end of data
//For subheaderA LENGTH need to be in range 0-255
struct frameSubheaderA createSubheaderA(int Num_value, int last_value, int LENGTH_value)
{
  //logger(NORMAL_LOG,__func__,"creating subheader A ");
  struct frameSubheaderA SubheaderA;
  SubheaderA.F = 0;
  SubheaderA.Num = Num_value;
  SubheaderA.L = last_value;
  SubheaderA.LENGTH = LENGTH_value;
  return SubheaderA;
}

//Subheader B has Foramat value = 1
//Num_value data block number 0-127 
//last_flag bit that inform if frame contains end of data
//For subheaderB LENGTH need to be in range 0-65535
struct frameSubheaderB createSubheaderB(int Num_value, int last_value, int LENGTH_value)
{
  //logger(NORMAL_LOG,__func__,"creating subheader B");
  struct frameSubheaderB SubheaderB;
  SubheaderB.F = 1;
  SubheaderB.Num = Num_value;
  SubheaderB.L = last_value;
  SubheaderB.LENGTH = LENGTH_value;
  return SubheaderB;
}

//a - ack n - nack
struct frameHSinfo createHSinfo(int UID_value, int NUM_value, int ack)
{
  //logger(NORMAL_LOG,__func__,"creating HandShake info ");
  struct frameHSinfo HSinfo;
  HSinfo.UID = UID_value;
  HSinfo.NUM = NUM_value;
  HSinfo.A = ack;

}

struct frameData createData(char *data)
{
  //logger(NORMAL_LOG,__func__,"creating data frame");
  struct frameData Data;
  Data.data = data;
  return Data;
}
//header variables:
//TI: 1-handshake,  2-connecting(login),    3-disconect(logout),    (4-9)-reserved, 10-asking for radio condition,   11-inforamtion with rodio condition status,    (12-30)-data transmision   31-test,
//N:  0 - new transmission 1-3 - retransmission
//Controle elements variables:
//UID:UID_value pass 0 on first transmission, on response pass generated UID 1-7
//C : coding value 0-3
//TUID: pass 0 to generate random tuid or pass generated earlier TUID 1-255
//Subheader variables:
//number of frame if data was splited into many frames
//last - 1 if last frame 0 - if another frame with data will be send
//LENGTH - amount of bits that data takes
//Data:
//data as string  
//HSinfo:
//UID: user id if user dont have id pass tuid
//NUM - number of transmission
//ack - 1- ack 0 - nack
frame generateFrame(int header_TI_value, int header_N_value, int controle_UID_value, int controle_C_value,
                    int controle_TUID_value, int subheader_Num_value, int subheader_last, char *data, int HSinfo_ack)
{
  //logger(NORMAL_LOG,__func__,"Starting generating frame");
  frame genFrame;
  struct frameHeader Header = createHeader(header_TI_value, header_N_value);
  if (Header.TI == 1)
  {
    int UID = controle_UID_value;
    if (controle_UID_value == 0)
    {
      UID = controle_TUID_value;
    }

    struct frameHSinfo HSinfo = createHSinfo(UID, header_N_value, HSinfo_ack);
    genFrame.handshakeFrame.frameHeader = Header;
    genFrame.handshakeFrame.frameHSinfo = HSinfo;
    return genFrame;
  }
  
  genFrame.dataFrame.frameHeader = Header;

  if (((Header.TI == 10 || Header.TI == 11) && controle_UID_value == 0) || Header.TI == 2)
  {
    genFrame.dataFrame.frameControlElements.frameControlElementsUnlogged = createControlElementsUnlogged(
        controle_UID_value, controle_C_value, controle_TUID_value);
  }
  else
  {
    genFrame.dataFrame.frameControlElements.frameControlElementsLogged = createControlElementsLogged(controle_UID_value,
                                                                                                     controle_C_value);
  }
  int LENGTH = strlen(data) * CHAR_BIT;
  if (LENGTH < SUBHEADER_A_MAX_VALUE)
  {

    genFrame.dataFrame.frameSubheader.frameSubheaderA = createSubheaderA(subheader_Num_value, subheader_last, LENGTH);
  }
  else
  {
    genFrame.dataFrame.frameSubheader.frameSubheaderB = createSubheaderB(subheader_Num_value, subheader_last, LENGTH);
  }

  genFrame.dataFrame.frameData = createData(data);

  return genFrame;
}
