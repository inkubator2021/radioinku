#ifndef __STR_TO_FRAME
#define __STR_TO_FRAME

#include "frameStructure.h"
#include "epollFunctions.h"
#include "dataFrameSize.h"
#include <stdio.h>
#include <stdlib.h>


#define BITS_IN_BYTE 8

frame string_to_frame(char *frame);

#endif // !__STR_TO_FRAME
