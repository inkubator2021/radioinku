#ifndef __RADIO
#define __RADIO

#include <stdlib.h>
#include <stdio.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <sys/types.h>
#include <errno.h>
#include <strings.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <fcntl.h>
#include <stdint.h>

#include "frameStructure.h"

#define PORT 8080
#define MAXBUF 1024
#define QUEUE_LIMIT 5

#define SERVER "127.0.0.1"

int recv_frame(int fd, unsigned char *buff);

int send_frame(int fd, unsigned char *buff, unsigned int bufferSize);

int epoll_create(int epoll_create_flag);

int epoll_control(int epoll_fd, int socket_fd, int state,
		  int epoll_flag);

int set_socket_to_non_block(int socket_fd, int sock_block_flag);

int frameInterpretation(frame frame);

#endif
