#ifndef __CEASAR
#define __CEASAR

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MIN_CHARACTER_VALUE 32
#define MAX_CHARACTER_VALUE 126
#define LEFT_SHIFT 3
#define RIGHT_SHIFT 2

char *ceasarCiphering(char *message);
char *ceasarDeciphering(char *message);

#endif // !__CEASAR
