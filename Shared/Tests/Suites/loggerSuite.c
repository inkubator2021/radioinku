#include <assert.h>
#include <stdio.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <stdlib.h>
#include "logger.h"
#include "loggerSuite.h"
//---------------------------------------------------------------
// PURPOSE:Verify get time  functionality
//
//  PREREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) function returns a pointer to a struct tm
//---------------------------------------------------------------
void test_getTime(void){
    printf("%s:%d\n", __func__, __LINE__);
    struct tm test_tm = getTime();
    assert(&test_tm != NULL);   
}

//---------------------------------------------------------------
// PURPOSE:Verify create directory functionality
//
//  PREREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) running funtion create directory in current directory
//  2) after assertion directory is beeing deleted
//---------------------------------------------------------------
void test_createDirectory(void){
    printf("%s:%d\n", __func__, __LINE__);
    assert(createDirectory()==0);
    struct stat sb;
    if (stat("./logs",&sb) == 0){
        rmdir("./logs");
    }
}

//---------------------------------------------------------------
// PURPOSE:Verify get time stamp functionality
//
//  PREREQUISITES:
//  a) get time test need to be tested
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) finding if function return pointer to char array
//  2) if char array have lenght between min and max possible 
//---------------------------------------------------------------
void test_getTimeStamp(void){
    printf("%s:%d\n", __func__, __LINE__);
    char *test_timeStamp = getTimeStamp();
    assert(test_timeStamp != NULL);
    assert(strlen(test_timeStamp) >= 0);
    assert(strlen(test_timeStamp) <= LOG_TIMESTAMP_SIZE);
    free(test_timeStamp);
}

//---------------------------------------------------------------
// PURPOSE:Verify concta message string functionality
//
//  PREREQUISITES:
//  a) none
//  
//  TRIGGER:
//  1) runing funtion with two strings
//  
//  VERIFICATION:
//  1) checking if value returned is not null
//---------------------------------------------------------------
void test_concatMessageString(void){
    printf("%s:%d\n", __func__, __LINE__);
    assert(concatMessageString("123","456")!=NULL);
    assert(concatMessageString("","456")!=NULL);    
}

//---------------------------------------------------------------
// PURPOSE:Verify get file name  functionality
//
//  PREREQUISITES:
//  a) concat message string must be tested
//  
//  TRIGGER:
//  1) runing funtion with int id
//  
//  VERIFICATION:
//  1) checking if returned value isnt null
//  2) checking if returned value has propper lenght = lenght of id + 11
//---------------------------------------------------------------
void test_getFileName(void){
    printf("%s:%d\n", __func__, __LINE__);
    char *test1 = getFileName(123);
    char *test2 = getFileName(1942903630);
    assert(test1 != NULL);
    assert(test2 != NULL);
    assert(strlen(test1) == 14);// ./logs/123.log
    assert(strlen(test2) == 21);
    free(test1);
    free(test2);
}

//---------------------------------------------------------------
// PURPOSE:Verify write to file functionality
//
//  PREREQUISITES:
//  a) directory ./logs must exists
//  
//  TRIGGER:
//  1) runing funtion with char message and int id 
//  
//  VERIFICATION:
//  1) If diretory exists funtion write message to file with time stamp given by time stamp funtion 
//  2) if file dont exists then funtion create it 
//  3) return -1 if funtion wasnt able to open file 
//---------------------------------------------------------------
void test_writeToFile(void){
    printf("%s:%d\n", __func__, __LINE__);
    struct stat sb;
        if (stat("./logs",&sb) == 0){
            system("rm -Rd logs");
        }
        assert(writeToFile("test message",123) == -1);
        createDirectory();
    if (stat("./logs",&sb) == 0){
        assert(writeToFile("test message",123) == 0);
    }

    
}
 
 //---------------------------------------------------------------
// PURPOSE:Verify get ID functionality
//
//  PREREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) runing function
//  
//  VERIFICATION:
//  1) Funtion return non zero rand int  
//---------------------------------------------------------------
void test_getID(void){
    printf("%s:%d\n", __func__, __LINE__);
    assert(getID() > 0);
}

//---------------------------------------------------------------
// PURPOSE:Verify logger init  functionality
//
//  PREREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) runing funtion
//  
//  VERIFICATION:
//  1) create diretory and then file named ./logs/(random id).log
//  2) append message Initial log to file 
//---------------------------------------------------------------
void test_loggerInit(void){
    printf("%s:%d\n", __func__, __LINE__);
    int testID = loggerInit("test");
    assert(testID > 0);
}

//---------------------------------------------------------------
// PURPOSE:Verify logger  functionality
//
//  PREREQUISITES:
//  a) diretory ./logs need to exists
//  
//  TRIGGER:
//  1) runing funtion with flag message id side
//  
//  VERIFICATION:
//  1) funntion assert message with propper flag and side opition to propper file
//  2) on wrong flag or side atribute -1 will be returned
//---------------------------------------------------------------
void test_logger(void){
    printf("%s:%d\n", __func__, __LINE__);
    struct stat sb;
        createDirectory();
    if (stat("./logs",&sb) == 0){
        unsigned int id = getID();
        assert(logger(WARNING_LOG,__func__,"test") == id);
        assert(logger(ERROR_LOG,__func__,"test") == id);
        assert(logger(NORMAL_LOG,__func__,"test") == id);
        assert(logger('k',__func__,"test") == -1);
        
    }
}

//---------------------------------------------------------------
// PURPOSE:Verify logger  functionality
//
//  PREREQUISITES:
//  a) diretory ./logs need to exists
//  
//  TRIGGER:
//  1) runing funtion with flag message id side
//  
//  VERIFICATION:
//  1) funntion assert message with propper flag and side opition to propper file
//  2) on wrong flag or side atribute -1 will be returned
//--------------------------------------------------------------
void test_getSide(void){
    printf("%s:%d\n", __func__, __LINE__);
    assert(getSide("test") == "test");
}

void test_getLoggerInstance(void){
    printf("%s:%d\n", __func__, __LINE__);
    assert(getLoggerInstance(1) > 0 );
}

void runTestsLogger(void){
    printf("\nLogger tests \n");
    test_getTime();
    printf("Test1\n");
    test_createDirectory();
    printf("Test2\n");
    test_getTimeStamp();
    printf("Test3\n");
    test_concatMessageString();
    printf("Test4\n");
    test_getFileName();
    printf("Test5\n");
    test_writeToFile();
    printf("Test6\n");
    test_getID();
    printf("Test7\n");
    test_loggerInit();
    printf("Test8\n");
    test_logger();
    printf("Test9\n");
    test_getSide();
    printf("Test10\n");
    test_getLoggerInstance();
}
