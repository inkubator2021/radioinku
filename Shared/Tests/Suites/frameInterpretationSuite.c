#include "frameInterpretationSuite.h"
#include <stdlib.h>
#include <stdio.h>


#define TI_Wrong 0
#define TI_Handshake 1
#define TI_Login 2
#define TI_Logout 3
#define TI_Reserved_Low 4
#define TI_Reserved_High 9
#define TI_AFRC 10
#define TI_IARC 11
#define TI_DataTransmission_Low 12
#define TI_DataTransmission_High 30
#define TI_Test 31




//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) handshake frame with:
//  b) TI set to 0b00001
//  c) A set to 1
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns 10 when handshake confirms correct receival and uncoding of message
//---------------------------------------------------------------

void testFrameInterpretationHandshake1()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameHeader.TI = TI_Handshake;
    FrameHeader.R = 0;
    FrameHeader.N = 0;

    FrameHSinfo.A = 1;
    FrameHSinfo.NUM = 4;
    FrameHSinfo.R = 0;
    FrameHSinfo.UID = 2;

    HandshakeFrame.frameHeader = FrameHeader;
    HandshakeFrame.frameHSinfo = FrameHSinfo;

    Frame.handshakeFrame = HandshakeFrame;
    assert(frameInterpretation(Frame) == correctHandshakeConfirmation);

}

//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) handshake frame with:
//  b) TI set to 0b00001
//  c) A set to 0
//  d) N set to 1
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns 11 when handshake demands 1st retransmission
//---------------------------------------------------------------


void testFrameInterpretationHandshake2()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameHeader.TI = TI_Handshake;
    FrameHeader.R = 0;
    FrameHeader.N = 0;

    FrameHSinfo.A = 0;
    FrameHSinfo.NUM = 4;
    FrameHSinfo.R = 0;
    FrameHSinfo.UID = 2;

    HandshakeFrame.frameHeader = FrameHeader;
    HandshakeFrame.frameHSinfo = FrameHSinfo;

    Frame.handshakeFrame = HandshakeFrame;
    assert(frameInterpretation(Frame) == start1stRetransmission);

}


//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) handshake frame with:
//  b) TI set to 0b00001
//  c) A set to 0
//  d) N set to 2
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns 12 when handshake demands 2nd retransmission
//---------------------------------------------------------------


void testFrameInterpretationHandshake3()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameHeader.TI = TI_Handshake;
    FrameHeader.R = 0;
    FrameHeader.N = 1;

    FrameHSinfo.A = 0;
    FrameHSinfo.NUM = 4;
    FrameHSinfo.R = 0;
    FrameHSinfo.UID = 2;

    HandshakeFrame.frameHeader = FrameHeader;
    HandshakeFrame.frameHSinfo = FrameHSinfo;

    Frame.handshakeFrame = HandshakeFrame;
    assert(frameInterpretation(Frame) == start2ndRetransmission);

}

//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) handshake frame with:
//  b) TI set to 0b00001
//  c) A set to 0
//  d) N set to 3
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns 13 when handshake demands another retransmission
//  2) It ends with establishing radio conditions
//---------------------------------------------------------------


void testFrameInterpretationHandshake4()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameHeader.TI = TI_Handshake;
    FrameHeader.R = 0;
    FrameHeader.N = 2;

    FrameHSinfo.A = 0;
    FrameHSinfo.NUM = 4;
    FrameHSinfo.R = 0;
    FrameHSinfo.UID = 2;

    HandshakeFrame.frameHeader = FrameHeader;
    HandshakeFrame.frameHSinfo = FrameHSinfo;

    Frame.handshakeFrame = HandshakeFrame;
    assert(frameInterpretation(Frame) == start3rdRetransmission);

}

//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) handshake frame with:
//  b) TI set to 0b00001
//  c) A set to 0
//  d) N set to something beside 0-3
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns -3 when retransmission number is out of range
//---------------------------------------------------------------


void testFrameInterpretationHandshake5()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameHeader.TI = TI_Handshake;
    FrameHeader.R = 0;
    FrameHeader.N = 3;

    FrameHSinfo.A = 0;
    FrameHSinfo.NUM = 4;
    FrameHSinfo.R = 0;
    FrameHSinfo.UID = 2;

    HandshakeFrame.frameHeader = FrameHeader;
    HandshakeFrame.frameHSinfo = FrameHSinfo;

    Frame.handshakeFrame = HandshakeFrame;
    assert(frameInterpretation(Frame) == retransmissionNumberError);

}


//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) handshake frame with:
//  b) R set to 1
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns -4 when reserved bit in header isn't set to 0
//---------------------------------------------------------------

void testFrameInterpretationReservedHeader()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameHeader.R = 1;
    HandshakeFrame.frameHeader = FrameHeader;
    HandshakeFrame.frameHSinfo = FrameHSinfo;

    Frame.handshakeFrame = HandshakeFrame;

    
    assert(frameInterpretation(Frame) == 4);
}


//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) handshake frame with:
//  b) R set to 1
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns -4 when reserved bit in HSinfo isn't set to 0
//---------------------------------------------------------------

void testFrameInterpretationReservedHSinfo()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    
    FrameHeader.TI = TI_Handshake;
    FrameHSinfo.R = 1;
    HandshakeFrame.frameHeader = FrameHeader;
    HandshakeFrame.frameHSinfo = FrameHSinfo;


    Frame.handshakeFrame = HandshakeFrame;
    assert(frameInterpretation(Frame) == 4);
}

//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) data frame with:
//  b) TI set to 00010
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns 2 when frame demands login
//---------------------------------------------------------------

void testFrameInterpretationLogin()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameControlElements.frameControlElementsUnlogged = FrameControlElementsUnlogged;
    FrameSubheader.frameSubheaderA = FrameSubHeaderA;
    Frame.dataFrame = DataFrame;

    Frame.dataFrame.frameHeader.R = 0;
    Frame.dataFrame.frameHeader.TI = TI_Login;
    assert(frameInterpretation(Frame) == startLogingIn);

}

//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) data frame with:
//  b) TI set to 00011
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns 3 when frame demands logout
//---------------------------------------------------------------

void testFrameInterpretationLogout()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameControlElements.frameControlElementsLogged = FrameControlElementsLogged;
    FrameSubheader.frameSubheaderA = FrameSubHeaderA;
    Frame.dataFrame = DataFrame;
    Frame.dataFrame.frameHeader.R = 0;
    Frame.dataFrame.frameHeader.TI = TI_Logout;
    assert(frameInterpretation(Frame) == startLogingOut);
}


//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) data frame with:
//  b) TI set to 00100 to 01001
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns 4 when frame uses reserved TI
//---------------------------------------------------------------

void testFrameInterpretationReserved()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameControlElements.frameControlElementsLogged = FrameControlElementsLogged;
    FrameSubheader.frameSubheaderA = FrameSubHeaderA;
    Frame.dataFrame = DataFrame;
    Frame.dataFrame.frameHeader.R = 0;
    for (int TI = TI_Reserved_Low; TI < TI_Reserved_High; TI++)
    {
        Frame.dataFrame.frameHeader.TI = TI;
        assert(frameInterpretation(Frame) == startReserved);
    }
}

//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) data frame with:
//  b) TI set to 01010
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns 5 when frame asks for radio conditions
//---------------------------------------------------------------

void testFrameInterpretationAFRC()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameControlElements.frameControlElementsLogged = FrameControlElementsLogged;
    FrameSubheader.frameSubheaderA = FrameSubHeaderA;
    Frame.dataFrame = DataFrame;
    Frame.dataFrame.frameHeader.R = 0;
    Frame.dataFrame.frameHeader.TI = TI_AFRC;
    assert(frameInterpretation(Frame) == startAFRC);
}

//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) data frame with:
//  b) TI set to 01011
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns 6 when frame responds with information about radio conditions
//---------------------------------------------------------------

void testFrameInterpretationIARC()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameControlElements.frameControlElementsLogged = FrameControlElementsLogged;
    FrameSubheader.frameSubheaderA = FrameSubHeaderA;
    Frame.dataFrame = DataFrame;
    Frame.dataFrame.frameHeader.R = 0;
    Frame.dataFrame.frameHeader.TI = TI_IARC;
    assert(frameInterpretation(Frame) == startIARC);
}

//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) data frame with:
//  b) TI set to 01100 to 11110
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns 7 when frame transmits data
//---------------------------------------------------------------

void testFrameInterpretationDataTransmission()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameControlElements.frameControlElementsLogged = FrameControlElementsLogged;
    FrameSubheader.frameSubheaderA = FrameSubHeaderA;
    Frame.dataFrame = DataFrame;
    Frame.dataFrame.frameHeader.R = 0;

    for(int TI = TI_DataTransmission_Low; TI < TI_DataTransmission_High; TI++)
    {
        Frame.dataFrame.frameHeader.TI = TI;
        assert(frameInterpretation(Frame) == startDataTransmission);
    }
   
}

//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) data frame with:
//  b) TI set to 01100 to 11110
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns 7 when frame transmits data
//---------------------------------------------------------------

void testFrameInterpretationTest()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameControlElements.frameControlElementsLogged = FrameControlElementsLogged;
    FrameSubheader.frameSubheaderA = FrameSubHeaderA;
    Frame.dataFrame = DataFrame;
    Frame.dataFrame.frameHeader.R = 0;

    Frame.dataFrame.frameHeader.TI = TI_Test;
    assert(frameInterpretation(Frame) == startTest);
    
}

//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) data frame with:
//  b) TI set to 0
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns -1 when frame has wrong TI
//---------------------------------------------------------------

void testFrameInterpretationWrongTypeIndicator()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameControlElements.frameControlElementsLogged = FrameControlElementsLogged;
    FrameSubheader.frameSubheaderA = FrameSubHeaderA;
    Frame.dataFrame = DataFrame;
    Frame.dataFrame.frameHeader.R = 0;

    Frame.dataFrame.frameHeader.TI = TI_Wrong;
    assert(frameInterpretation(Frame) == typeIndicatorOutOfRange);
    
}


//---------------------------------------------------------------
// PURPOSE:Verify frameInterpretation function
//
//  PREREQUISITES:
//  a) data frame with:
//  b) TI set to 0
//  c) UID set to non 0 value
//  
//  TRIGGER:
//  1) running function
//  
//  VERIFICATION:
//  1) Function returns -5 when user wants to log in when he's already logged
//---------------------------------------------------------------

void testFrameInterpretationLoggingInWhenLogged()
{
    struct frameHeader FrameHeader;
    struct frameControlElementsUnlogged FrameControlElementsUnlogged;
    struct frameControlElementsLogged FrameControlElementsLogged;
    union frameControlElements FrameControlElements;
    struct frameSubheaderA FrameSubHeaderA;
    struct frameSubheaderB FrameSubHeaderB;
    union frameSubheader FrameSubheader;
    struct frameData FrameData;
    struct frameHSinfo FrameHsinfo;
    struct dataFrame DataFrame;;
    struct frameHSinfo FrameHSinfo;
    struct handshakeFrame HandshakeFrame;
    union frame Frame;

    FrameControlElements.frameControlElementsLogged = FrameControlElementsLogged;
    FrameSubheader.frameSubheaderA = FrameSubHeaderA;
    Frame.dataFrame = DataFrame;
    Frame.dataFrame.frameHeader.R = 0;

    Frame.dataFrame.frameHeader.TI = TI_Login;
    Frame.dataFrame.frameControlElements.frameControlElementsLogged.UID = 2;
    assert(frameInterpretation(Frame) == loginError);
    
}


void runFrameInterpretationTests()
{
    testFrameInterpretationHandshake1();
    testFrameInterpretationHandshake2();
    testFrameInterpretationHandshake3();
    testFrameInterpretationHandshake4();
    testFrameInterpretationHandshake5();
    testFrameInterpretationReservedHeader();
    testFrameInterpretationReservedHSinfo();
    testFrameInterpretationLogin();
    testFrameInterpretationLogout();
    testFrameInterpretationReserved();
    testFrameInterpretationAFRC();
    testFrameInterpretationIARC();
    testFrameInterpretationDataTransmission();
    testFrameInterpretationTest();
    testFrameInterpretationWrongTypeIndicator();
    testFrameInterpretationLoggingInWhenLogged();
}
