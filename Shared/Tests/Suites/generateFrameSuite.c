#include "generateFrameSuite.h"
#include "generateFrame.h"
#include <string.h>
#include <assert.h>
#include "frameStructure.h"

//---------------------------------------------------------------
// PURPOSE:Verify create header  functionality
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function and creating struct 
//  
//  VERIFICATION:
//  1) created structure have fields occupied with variables
//---------------------------------------------------------------
void test_createHeader(void){
    printf("%s:%d\n", __func__, __LINE__);
    struct frameHeader testHeader = createHeader(31,0);
    assert(&testHeader != NULL);
    assert(testHeader.TI == 31);
    assert(testHeader.N == 0);
    assert(testHeader.R == 0);
}

//---------------------------------------------------------------
// PURPOSE:Verify getRandInt  functionality
//
//  PREQUISITES:
//  a) passing max value as variale 
//  
//  TRIGGER:
//  1) running function 
//  
//  VERIFICATION:
//  1) fuction returns value between 1 - variable tah was passed
//---------------------------------------------------------------
void test_getRandInt(void){
    printf("%s:%d\n", __func__, __LINE__);
    int testRandInt = getRandInt(10);
    assert(testRandInt < 11 && testRandInt > 0 );
}

//---------------------------------------------------------------
// PURPOSE:Verify create control emement unlogged  functionality
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function and creating struct 
//  
//  VERIFICATION:
//  1) created structure have fields occupied with variables
//---------------------------------------------------------------
void test_createControlElementsUnlogged(void){
    printf("%s:%d\n", __func__, __LINE__);
    struct frameControlElementsUnlogged testControlElementsUnlogged = createControlElementsUnlogged(0,0,0);
    printf("%s:%d\n", __func__, __LINE__);
    assert(testControlElementsUnlogged.UID == 0);
    assert(testControlElementsUnlogged.C == 0);
    assert(testControlElementsUnlogged.CC > 0 && testControlElementsUnlogged.CC < 4);
    assert(testControlElementsUnlogged.R == 0);
    assert(testControlElementsUnlogged.RESERVED == 0);
    assert(testControlElementsUnlogged.TUID > 0 && testControlElementsUnlogged.TUID < 256);
    struct frameControlElementsUnlogged testControlElementsUnlogged2 = createControlElementsUnlogged(0,0,1);
    assert(testControlElementsUnlogged2.TUID == 1);
}

//---------------------------------------------------------------
// PURPOSE:Verify create control emement logged  functionality
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function and creating struct 
//  
//  VERIFICATION:
//  1) created structure have fields occupied with variables
//---------------------------------------------------------------
void test_createControlElementsLogged(void){
    printf("%s:%d\n", __func__, __LINE__);
    struct frameControlElementsLogged testControlElementsLogged = createControlElementsLogged(1,2);
    assert(testControlElementsLogged.UID == 1);
    assert(testControlElementsLogged.C == 2);
    assert(testControlElementsLogged.CC > 0 && testControlElementsLogged.CC < 4);
    assert(testControlElementsLogged.R == 0);
    assert(testControlElementsLogged.RESERVED == 0);
}    

//---------------------------------------------------------------
// PURPOSE:Verify create subheaderA  functionality
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function and creating struct 
//  
//  VERIFICATION:
//  1) created structure have fields occupied with variables
//---------------------------------------------------------------
void test_createSubheaderA(void){
    printf("%s:%d\n", __func__, __LINE__);
    struct frameSubheaderA testSubheaderA1 = createSubheaderA(0,1,5);
    assert(testSubheaderA1.F == 0);
    assert(testSubheaderA1.Num == 0);
    assert(testSubheaderA1.L == 1);
    assert(testSubheaderA1.LENGTH == 5); 
    struct frameSubheaderA testSubheaderA2 = createSubheaderA(0,0,5);
    assert(testSubheaderA2.L == 0);
}
//---------------------------------------------------------------
// PURPOSE:Verify create subheaderB  functionality
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function and creating struct 
//  
//  VERIFICATION:
//  1) created structure have fields occupied with variables
//---------------------------------------------------------------
void test_createSubheaderB(void){
    printf("%s:%d\n", __func__, __LINE__);
    struct frameSubheaderB testSubheaderB1 = createSubheaderB(0,1,5);
    assert(testSubheaderB1.F == 1);
    assert(testSubheaderB1.Num == 0);
    assert(testSubheaderB1.L == 1);
    assert(testSubheaderB1.LENGTH == 5); 
    struct frameSubheaderB testSubheaderB2 = createSubheaderB(0,0,5);
    assert(testSubheaderB2.L == 0);
    
}
//---------------------------------------------------------------
// PURPOSE:Verify create HSinfo  functionality
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function and creating struct 
//  
//  VERIFICATION:
//  1) created structure have fields occupied with variables
//---------------------------------------------------------------
void test_createHSinfo(void){
    printf("%s:%d\n", __func__, __LINE__);
    struct frameHSinfo testHSinfo1 = createHSinfo(0,0,1);
    assert(testHSinfo1.NUM == 0);
    assert(testHSinfo1.A == 0);
    struct frameHSinfo testHSinfo2 = createHSinfo(0,0,0);
    assert(testHSinfo2.A == 0);
}
//---------------------------------------------------------------
// PURPOSE:Verify create createData functionality
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function and creating struct 
//  
//  VERIFICATION:
//  1) created structure have fields occupied with variables
//---------------------------------------------------------------
void test_createData(void){
    printf("%s:%d\n", __func__, __LINE__);
    // struct frameData testData = createData("hello");
    // assert(testData.data == "hello");
}
//---------------------------------------------------------------
// PURPOSE:Verify generate frame  functionality
//
//  PREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) running function and creating struct 
//  
//  VERIFICATION:
//  1) created structure have fields occupied with variables
//---------------------------------------------------------------
void test_generateFrame(void){
    printf("%s:%d\n", __func__, __LINE__);
    frame testgenFrame1 = generateFrame(1,0,0,0,0,0,0,"data",1);
    assert(testgenFrame1.handshakeFrame.frameHeader.TI == 1);
    frame testgenFrame2 = generateFrame(2,0,0,0,0,0,0,"data",1);
    assert(testgenFrame2.dataFrame.frameHeader.TI == 2);
    frame testgenFrame3 = generateFrame(3,0,0,0,0,0,0,"a",1);
    assert(testgenFrame3.dataFrame.frameSubheader.frameSubheaderA.LENGTH == 8);
    frame testgenFrame4 = generateFrame(3,0,0,0,0,0,0,"12345678901234567890123456789012345678901234567890",1);
    assert(testgenFrame4.dataFrame.frameSubheader.frameSubheaderB.LENGTH == 400);
}




void runTestsGenerateFrame(void){
    printf("\nGenerate frame tests\n");
    printf("Test1\n");
    test_createHeader();
    printf("Test2\n");
    test_getRandInt();
    printf("Test3\n");
    test_createControlElementsUnlogged();
    printf("Test4\n");
    test_createControlElementsLogged();
    printf("Test5\n");
    test_createSubheaderA();
    printf("Test6\n");
    test_createSubheaderB();
    printf("Test7\n");
    test_createHSinfo();
    printf("Test8\n");
    test_createData();
    printf("Test9\n");
    test_generateFrame();
    printf("\n");
}
