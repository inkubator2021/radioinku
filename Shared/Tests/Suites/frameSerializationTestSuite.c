#include "frameSerializationTestSuite.h"

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function frameIntoBuffer and all _serial functions this function contains
//by creating buffer with dataframe(subheader A, logged)
//and checking its proper size

//PREREQUISITIES:
//a) specific elements of frame we want to send should be created and properly filled
//   in this case:
//   TestFrame.dataFrame.frameHeader.TI > 0 - to create dataframe
//   TestFrame.dataFrame.frameControlElements.frameControlElementsLogged.UID > 0 - user logged
//   TestFrame.dataFrame.frameSubheader.frameSubheaderA.F = 0 - subheader A
//   TestFrame.dataFrame.frameData.data = malloc(sizeof(char) * size); - data allocated

//TRIGGER:
//1) Running frameIntoBuffer function

//VERIFICATION
//1) Function frameIntoBuffer returns proper size of buffer
//-------------------------------------------------------------------------------------------------
void test_1_frame_into_buffer(void)
{
  printf("%s:%d\n", __func__, __LINE__);

  char *buffer;
  uint16_t bufferSize = 0;
  frame TestFrame;

  //header - size 1
  TestFrame.dataFrame.frameHeader.TI = 16; //OTHER THAN HS
  TestFrame.dataFrame.frameHeader.N = 2;
  TestFrame.dataFrame.frameHeader.R = 1;

  //frame control elements - size 2
  TestFrame.dataFrame.frameControlElements.frameControlElementsLogged.UID = 7;
  TestFrame.dataFrame.frameControlElements.frameControlElementsLogged.C = 0;
  TestFrame.dataFrame.frameControlElements.frameControlElementsLogged.CC = 0;
  TestFrame.dataFrame.frameControlElements.frameControlElementsLogged.R = 1;
  TestFrame.dataFrame.frameControlElements.frameControlElementsLogged.RESERVED = 15;

  //frameSubheaderA - size 2 - UINT8_T
  TestFrame.dataFrame.frameSubheader.frameSubheaderA.F = 0;
  TestFrame.dataFrame.frameSubheader.frameSubheaderA.Num = 63;
  TestFrame.dataFrame.frameSubheader.frameSubheaderA.L = 0;
  TestFrame.dataFrame.frameSubheader.frameSubheaderA.LENGTH = 32;

  //data frame_size 6 (padding included)
  uint16_t length = TestFrame.dataFrame.frameSubheader.frameSubheaderA.LENGTH;
  uint16_t dataSizeBytes = length / BITS_IN_BYTE;
  uint16_t frameData_size = calculate_dataFrame_size(length) / BITS_IN_BYTE;
  uint8_t padding_size = frameData_size - dataSizeBytes;

  TestFrame.dataFrame.frameData.data = malloc(sizeof(char) * dataSizeBytes);
  TestFrame.dataFrame.frameData.padding = malloc(sizeof(char) * padding_size);
  TestFrame.dataFrame.frameData.data[0] = 0x38;
  TestFrame.dataFrame.frameData.data[1] = 0xA7;
  TestFrame.dataFrame.frameData.data[2] = 0xB4;
  TestFrame.dataFrame.frameData.data[3] = 0x3C;

  buffer = frameIntoBuffer(TestFrame, &bufferSize);


  assert(bufferSize == 11);

  free(buffer);

  printf("Test test_1_frame_into_buffer passed\n\n");
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function frameIntoBuffer and all _serial functions this function contains
//by creating buffer with dataframe(subheader B, unlogged, data=NULL)
//and checking its proper size

//PREREQUISITIES:
//a) specific elements of frame we want to send should be created and properly filled
//   in this case:
//   TestFrame.dataFrame.frameHeader.TI > 0 - to create dataframe
//   TestFrame.dataFrame.frameControlElements.frameControlElementsLogged.UID = 0 - user unlogged
//   TestFrame.dataFrame.frameSubheader.frameSubheaderA.F = 1 - subheader B
//   TestFrame.dataFrame.frameData.data = NULL

//TRIGGER:
//1) Running frameIntoBuffer function

//VERIFICATION
//1) Function frameIntoBuffer returns proper size of buffer
//-----------------------------------------------------------------------------------------------
void test_2_frame_into_buffer(void)
{
  printf("%s:%d\n", __func__, __LINE__);

  char *buffer;
  uint16_t bufferSize = 0;
  frame TestFrame;

  //header - size 1
  TestFrame.dataFrame.frameHeader.TI = 16;
  TestFrame.dataFrame.frameHeader.N = 2;
  TestFrame.dataFrame.frameHeader.R = 1;

  //frame control elements - size 3
  TestFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.UID = 0;
  TestFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.C = 0;
  TestFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.CC = 0;
  TestFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.R = 1;
  TestFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.TUID = 1;
  TestFrame.dataFrame.frameControlElements.frameControlElementsUnlogged.RESERVED = 15;

  //frameSubheaderA - size 3 - UINT16_T
  TestFrame.dataFrame.frameSubheader.frameSubheaderB.F = 1;
  TestFrame.dataFrame.frameSubheader.frameSubheaderB.Num = 63;
  TestFrame.dataFrame.frameSubheader.frameSubheaderB.L = 0;
  TestFrame.dataFrame.frameSubheader.frameSubheaderB.LENGTH = 32;

  //data = NULL
  TestFrame.dataFrame.frameData.data = NULL;

  buffer = frameIntoBuffer(TestFrame, &bufferSize);

  assert(bufferSize == 7);

  free(buffer);
  printf("Test test_2_frame_into_buffer passed\n\n");
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function frameIntoBuffer and all _serial functions this function contains
//by creating buffer with handshakeFrame
//and checking its proper size

//PREREQUISITIES:
//a) specific elements of frame we want to send should be created and properly filled
//   in this case:
//   TestFrame.dataFrame.frameHeader.TI > 0 - to create dataframe
//   TestFrame.dataFrame.frameControlElements.frameControlElementsLogged.UID = 0 - user unlogged
//   TestFrame.dataFrame.frameSubheader.frameSubheaderA.F = 1 - subheader B
//   TestFrame.dataFrame.frameData.data = NULL

//TRIGGER:
//1) Running frameIntoBuffer function

//VERIFICATION
//1) Function frameIntoBuffer returns proper size of buffer
//-----------------------------------------------------------------------------------------------
void test_3_frame_into_buffer(void)
{
  printf("%s:%d\n", __func__, __LINE__);

  char *buffer;
  uint16_t bufferSize = 0;
  frame TestFrame;

  //header - size 1
  TestFrame.handshakeFrame.frameHeader.TI = 1;
  TestFrame.handshakeFrame.frameHeader.N = 2;
  TestFrame.handshakeFrame.frameHeader.R = 1;

  //handshakeframe - size 2
  TestFrame.handshakeFrame.frameHSinfo.UID = 0;
  TestFrame.handshakeFrame.frameHSinfo.NUM = 0;
  TestFrame.handshakeFrame.frameHSinfo.A = 0;
  TestFrame.handshakeFrame.frameHSinfo.R = 0;

  buffer = frameIntoBuffer(TestFrame, &bufferSize);

  assert(bufferSize == 3);

  free(buffer);

  printf("Test test_3_frame_into_buffer passed\n\n");
}

void frame_serialization_tests(void)
{
  test_1_frame_into_buffer();
  test_2_frame_into_buffer();
  test_3_frame_into_buffer();
}
