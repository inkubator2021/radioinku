#include "dataFrameSizeTestSuite.h"

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function calculate_dataFrame_size

//PREREQUISITIES:
//a) data size <= frame size

//TRIGGER:
//1) Running function calculate_dataFrame_size

//VERIFICATION
//1) Function calculate_dataFrame_size returns proper data frame size
//--------------------------------------------------------------------------------------------------
#define MAX_N 6
#define size_r_mock 10
void test_1_calculate_dataFrame_size(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  uint16_t size[MAX_N] = { 16, 48, 144, 432, 1296, 3888 };
  int ret;
  ret = calculate_dataFrame_size(size_r_mock);

  assert(ret == size[0]);
  printf("Test test_1_calculate_dataFrame_size passed\n\n");
}
#undef size_r_mock

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function calculate_dataFrame_size

//PREREQUISITIES:
//a) data size > biggest frame size

//TRIGGER:
//1) Running function calculate_dataFrame_size

//VERIFICATION
//1) Function calculate_dataFrame_size returns proper data frame size
//--------------------------------------------------------------------------------------------------
#define size_r_mock 4000
void test_2_calculate_dataFrame_size(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  uint16_t size[MAX_N] =
  { 16, 48, 144, 432, 1296, 3888 };
  int ret;
  ret = calculate_dataFrame_size(size_r_mock);

  assert(ret == size[MAX_N - 1]);
  printf("Test test_2_calculate_dataFrame_size passed\n\n");
}
#undef size_r_mock

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function calculate_dataFrame_size

//PREREQUISITIES:
//a) padding size > 0.4 * frame size

//TRIGGER:
//1) Running function calculate_dataFrame_size

//VERIFICATION
//1) Function calculate_dataFrame_size returns proper data frame size
//--------------------------------------------------------------------------------------------------
#define size_r_mock 25
void test_3_calculate_dataFrame_size(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  uint16_t size[MAX_N] =
  { 16, 48, 144, 432, 1296, 3888 };
  int ret;
  ret = calculate_dataFrame_size(size_r_mock);

  assert(ret == size[0]);
  printf("Test test_3_calculate_dataFrame_size passed\n\n");
}
#undef size_r_mock
#undef MAX_N

void calculateDataFrameSizeTests(void)
{
  test_1_calculate_dataFrame_size();
  test_2_calculate_dataFrame_size();
  test_3_calculate_dataFrame_size();
}
