#ifndef frameInterpretationTests
#define frameInterpretationTests
#include <assert.h>
#include "frameInterpretation.h"
#include "frameStructure.h"

void runFrameInterpretationTests();
void testFrameInterpretationHandshake1();
void testFrameInterpretationHandshake2();
void testFrameInterpretationHandshake3();
void testFrameInterpretationHandshake4();
void testFrameInterpretationHandshake5();
void testFrameInterpretationReservedHeader();
void testFrameInterpretationReservedHSinfo();
void testFrameInterpretationLogin();
void testFrameInterpretationLogout();
void testFrameInterpretationReserved();
void testFrameInterpretationAFRC();
void testFrameInterpretationIARC();
void testFrameInterpretationDataTransmission();
void testFrameInterpretationTest();
void testFrameInterpretationwrongTypeIndicator();
void testFrameInterpretationLoggingInWhenLogged();

#endif