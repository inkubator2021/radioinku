#include "ceasarCipherTestSuite.h"

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function ceasarCiphering with known resulut

//PREREQUISITIES:
//a) Char* message "agat"

//TRIGGER:
//1) Call function with message from a)

//VERIFICATION
//1) Result is equal "dder"
//-------------------------------------------------------------------------------------------------
void ceasarCipherTest1()
{
    printf("%s:%d\n", __func__, __LINE__);
    char *message = "agat";
    char *cipheredMessage = ceasarCiphering(message);
    assert(!strcmp(cipheredMessage, "dder"));
    free(cipheredMessage);
    printf("%s done\n", __func__);
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function ceasarCiphering and ceasarDeciphering, odd number of characters

//PREREQUISITIES:
//a) Char* message with odd number of characters, last character should be one from middle part of
//  ASCII table (not out of range after shift)

//TRIGGER:
//1) Call ceasarCiphering function with message from a)
//2) Call ceasarDeciphering function with result from 1)
//3) Compare message from a) to result from 2)

//VERIFICATION
//1) Strings are equal
//-------------------------------------------------------------------------------------------------
void ceasarCipherTest2()
{
    printf("%s:%d\n", __func__, __LINE__);
    char *message = "~!@ B";
    char *cipheredMessage = ceasarCiphering(message);
    char* decipheredMessage = ceasarDeciphering(cipheredMessage);
    assert(!strcmp(decipheredMessage, message));
    free(cipheredMessage);
    free(decipheredMessage);
    printf("%s done\n", __func__);
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function ceasarCiphering and ceasarDeciphering, odd number of characters

//PREREQUISITIES:
//a) Char* message with odd number of characters, last character should be last character from
//  ASCII table (out of range after shift)

//TRIGGER:
//1) Call ceasarCiphering function with message from a)
//2) Call ceasarDeciphering function with result from 1)
//3) Compare message from a) to result from 2)

//VERIFICATION
//1) Strings are equal
//-------------------------------------------------------------------------------------------------
void ceasarCipherTest3()
{
    printf("%s:%d\n", __func__, __LINE__);
    char *message = "Tygrys~";
    char *cipheredMessage = ceasarCiphering(message);
    char *decipheredMessage = ceasarDeciphering(cipheredMessage);
    assert(!strcmp(decipheredMessage, message));
    free(cipheredMessage);
    free(decipheredMessage);
    printf("%s done\n", __func__);
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function ceasarCiphering and ceasarDeciphering, even number of characters

//PREREQUISITIES:
//a) Char* message with even number of characters

//TRIGGER:
//1) Call ceasarCiphering function with message from a)
//2) Call ceasarDeciphering function with result from 1)
//3) Compare message from a) to result from 2)

//VERIFICATION
//1) Strings are equal
//-------------------------------------------------------------------------------------------------
void ceasarCipherTest4()
{
    printf("%s:%d\n", __func__, __LINE__);
    char *message = "tygrysek";
    char *cipheredMessage = ceasarCiphering(message);
    char *decipheredMessage = ceasarDeciphering(cipheredMessage);
    assert(!strcmp(decipheredMessage, message));
    free(cipheredMessage);
    free(decipheredMessage);
    printf("%s done\n", __func__);
}

void runCeasarCipherTests()
{
    ceasarCipherTest1();
    ceasarCipherTest2();
    ceasarCipherTest3();
    ceasarCipherTest4();
}