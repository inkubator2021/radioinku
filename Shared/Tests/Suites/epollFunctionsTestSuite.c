#include "epollFunctionsTestSuite.h"
#include "sharedTestsWrappers.h"

extern bool mock_epoll_ctl;
extern bool mock_recv;

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function set_socket_to_non_block - success

//PREREQUISITIES:
//a) None

//TRIGGER:
//1) Running set_socket_to_non_block with invalid socket fd

//VERIFICATION
//1) Function set_socket_to_non_block return value is 1.
//-------------------------------------------------------------------------------------------------
#define socket_fd 1
void test_1_set_socket_to_non_block(void)
{
  printf("%s:%d\n", __func__, __LINE__);

  int ret = set_socket_to_non_block(socket_fd, F_SETFL);
  assert(ret == 1);
  printf("Test test_1_set_socket_to_non_block\n\n");
}
#undef socket_fd

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function set_socket_to_non_block - function failure

//PREREQUISITIES:
//a) None

//TRIGGER:
//1) Running set_socket_to_non_block with invalid socket fd

//VERIFICATION
//1) Function set_socket_to_non_block return value is -1.
//-------------------------------------------------------------------------------------------------
#define socket_fd -1
void test_2_set_socket_to_non_block(void)
{
  printf("%s:%d\n", __func__, __LINE__);

  int ret = set_socket_to_non_block(socket_fd, F_SETFL);
  assert(ret == -1);
  printf("Test test_1_set_socket_to_non_block\n\n");
}
#undef socket_fd

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function epoll_create - success

//PREREQUISITIES:
//a) None

//TRIGGER:
//1) Running function epoll_create

//VERIFICATION
//1) Function epoll_create return value >= 0
//-------------------------------------------------------------------------------------------------
void test_1_epoll_create(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  int ret = epoll_create(0);
  assert(ret >= 0);
  printf("Test test_1_epoll_create passed\n\n");
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function epoll_create - function failure

//PREREQUISITIES:
//a) None

//TRIGGER:
//1) Running function epoll_create with invalid argument

//VERIFICATION
//1) Function epoll_create return value is -1.
//-------------------------------------------------------------------------------------------------
#define epoll_flag_mock -1
void test_2_epoll_create(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  int ret = epoll_create(epoll_flag_mock);
  assert(ret == -1);
  printf("Test test_2_epoll_create passed\n\n");
}
#undef epoll_flag_mock

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function epoll_control

//PREREQUISITIES:
//a) None

//TRIGGER:
//1) Running function epoll_control

//VERIFICATION
//1) Function epoll_control return value is = 1.
//-------------------------------------------------------------------------------------------------
#define epoll_fd_mock 1
#define socket_fd_mock 1
void test_1_epoll_control(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  int ret;
  ret = epoll_control(epoll_fd_mock, socket_fd_mock, EPOLLIN, EPOLL_CTL_ADD);
  assert(ret == -1);
  printf("Test test_1_epoll_control passed\n\n");
}
#undef epoll_fd_mock
#undef socket_fd_mock

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function epoll_control - function failure

//PREREQUISITIES:
//a) None

//TRIGGER:
//1) Running function epoll_control with invalid epoll_fd argument

//VERIFICATION
//1) Function epoll_control return value is -1.
//-------------------------------------------------------------------------------------------------
#define epoll_fd_mock -1
#define socket_fd_mock 1
void test_2_epoll_control(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  int ret;
  ret = epoll_control(epoll_fd_mock, socket_fd_mock, EPOLLIN, EPOLL_CTL_ADD);
  assert(ret == -1);
  printf("Test test_1_epoll_control passed\n\n");
}
#undef epoll_fd_mock
#undef socket_fd_mock

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function send_frame - invalid arg

//PREREQUISITIES:
//a) buff == NULL

//TRIGGER:
//1) Running function send_frame

//VERIFICATION
//1) Function send_frame returns -1
//--------------------------------------------------------------------------------------------------
void test_1_send_frame(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  unsigned char *buff = NULL;
  uint16_t bufferSize;
  int fd = 1;

  int ret;
  ret = send_frame(fd, buff, bufferSize);

  assert(ret == -1);
  printf("Test %s passed\n\n", __func__);
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function send_frame - invalid arg

//PREREQUISITIES:
//a) buff == NULL

//TRIGGER:
//1) Running function send_frame

//VERIFICATION
//1) Function send_frame returns -1
//--------------------------------------------------------------------------------------------------
void test_2_send_frame(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  unsigned char *buff = malloc(sizeof(char) * MAXBUF);
  *buff = "A";
  uint16_t bufferSize;
  int fd = -8;

  int ret;
  ret = send_frame(fd, buff, bufferSize);

  assert(ret == -1);
  printf("Test %s passed\n\n", __func__);
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function send_frame - valid args

//PREREQUISITIES:
//a) passing valid arguments

//TRIGGER:
//1) Running function send_frame

//VERIFICATION
//1) Function send_frame returns -1
//--------------------------------------------------------------------------------------------------
void test_3_send_frame(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  unsigned char *buff = "A";
  uint16_t bufferSize;
  int fd = 1;

  int ret;
  ret = send_frame(fd, buff, bufferSize);

  assert(ret == -1);
  printf("Test %s passed\n\n", __func__);
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function recv_frame - ret = 0 - read complete

//PREREQUISITIES:
//a) recv function wrap
//b) function close wrap

//TRIGGER:
//1) Running function recv_frame

//VERIFICATION
//1) Function recv_frame returns 0
//--------------------------------------------------------------------------------------------------
void test_1_recv_frame(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  mock_recv = true;
  unsigned char *buff = "A";
  int fd = 1;
  int ret;

  ret = recv_frame(fd, buff);

  assert(ret == 0);
  mock_recv = false;
  printf("Test %s passed\n\n", __func__);
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function recv_frame - ret > 0 - message from socket received

//PREREQUISITIES:
//a) recv function wrap

//TRIGGER:
//1) Running function recv_frame

//VERIFICATION
//1) Function recv_frame returns value > 0.
//--------------------------------------------------------------------------------------------------

void test_2_recv_frame(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  mock_recv = true;
  unsigned char *buff = "A" ;
  int fd = 2;
  int ret;

  ret = recv_frame(fd, buff);

  assert(ret > 0);
  mock_recv = false;
  printf("Test %s passed\n\n", __func__);
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function recv_frame - error - return value < 0

//PREREQUISITIES:
//a) recv function wrap - errno = EAGAIN

//TRIGGER:
//1) Running function recv_frame

//VERIFICATION
//1) Function recv_frame returns value < 0.
//--------------------------------------------------------------------------------------------------
void test_3_recv_frame(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  mock_recv = true;
  unsigned char *buff = "A";
  int fd = -1;
  int ret;

  ret = recv_frame(fd, buff);

  assert(ret < 0);
  mock_recv = false;
  printf("Test %s passed\n\n", __func__);
}

//-------------------------------------------------------------------------------------------------
//PURPOSE: Verify function recv_frame - error - return value < 0

//PREREQUISITIES:
//a) passing invalid arguments
//b) function close wrap

//TRIGGER:
//1) Running function recv_frame

//VERIFICATION
//1) Function recv_frame returns value < 0.
//--------------------------------------------------------------------------------------------------
void test_4_recv_frame(void)
{
  printf("%s:%d\n", __func__, __LINE__);
  unsigned char *buff = NULL;
  int fd = -1;

  int ret;
  ret = recv_frame(fd, buff);

  assert(ret < 0);
  printf("Test %s passed\n\n", __func__);
}


void epollFunctionsTests(void)
{
  test_1_set_socket_to_non_block();
  test_2_set_socket_to_non_block();

  test_1_epoll_create();
  test_2_epoll_create();

  test_1_epoll_control();
  test_2_epoll_control();

  test_1_send_frame();
  test_2_send_frame();
  test_3_send_frame();

  test_1_recv_frame();
  test_2_recv_frame();
  test_3_recv_frame();
  test_4_recv_frame();
}
