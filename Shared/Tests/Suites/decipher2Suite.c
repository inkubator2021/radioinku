#include "decipher2Suite.h"


//---------------------------------------------------------------
// PURPOSE:Verify ciphering function's output's length
//
//  PREREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) calling function
//  
//  VERIFICATION:
//  1) Function returns output which should be 3x shorter than input
//  Otherwise function returns "error" string
//---------------------------------------------------------------


void testDecipherOutputLength()
{
    char *input = "1234";
    char *output = decipher2(input);
    assert(strcmp(output, "error") == 0);
    printf("testDecipherOutputLength passed\n");
}

//---------------------------------------------------------------
// PURPOSE:Verify deciphering function's output's
//
//  PREREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) calling function
//  
//  VERIFICATION:
//  1) Function returns deciphered output
//---------------------------------------------------------------


void testDecipher2()
{
    char *input = "243341411431342";
    char *output = decipher2(input);
    assert(strcmp(output, "Jakub") == 0);
    printf("testDecipher2 passed\n");
}


void runDecipher2Tests()
{
    testDecipherOutputLength();
    testDecipher2();
    printf("Decipher2Tests Passed\n");  
}
