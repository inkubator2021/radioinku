#ifndef __CEASAR_SUITE
#define __CEASAR_SUITE

#include <assert.h>
#include "ceasarCipher.h"

void ceasarCipherTest1();
void ceasarCipherTest2();
void ceasarCipherTest3();
void ceasarCipherTest4();
void runCeasarCipherTests();

#endif // !__CEASAR_SUITE