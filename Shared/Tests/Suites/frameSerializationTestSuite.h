#ifndef __FRAME_SERIALIZATION_TEST
#define __FRAME_SERIALIZATION_TEST

#include <assert.h>
#include "frameSerialization.h"

void test_1_frame_into_buffer(void);

void test_2_frame_into_buffer(void);

void test_3_frame_into_buffer(void);

void frame_serialization_tests(void);

#endif
