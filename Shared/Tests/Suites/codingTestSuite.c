#include "codingTestSuite.h"

//---------------------------------------------------------------
// PURPOSE: Verify messegeCodding function - checking error handling
//
//  PREREQUISITES:
//  a) None
//
//  TRIGGER:
//  1) calling function with message pointed to NULL
//
//  VERIFICATION:
//  1) function returns NULL pointer
//---------------------------------------------------------------
void messegeCoddingTest1()
{
  printf("%s:%d\n", __func__, __LINE__);

  unsigned char *messageMock = NULL;
  uint8_t codingLevelMock = 1;
  unsigned char *ret = messegeCodding(messageMock, codingLevelMock);

  assert(ret == NULL);

  printf("Test %s passed\n\n", __func__);
}

//---------------------------------------------------------------
// PURPOSE: Verify messegeCodding function - checking error handling
//  PREREQUISITES:
//  a) None
//
//  TRIGGER:
//  1) calling function with invalid codingLevel
//
//  VERIFICATION:
//  1) function returns NULL pointer
//---------------------------------------------------------------
void messegeCoddingTest2()
{
  printf("%s:%d\n", __func__, __LINE__);

  unsigned char *messageMock;
  messageMock = "~~";

  uint8_t codingLevelMock = 18;
  unsigned char *ret = messegeCodding(messageMock, codingLevelMock);

  assert(ret == NULL);

  printf("Test %s passed\n\n", __func__);
}

//---------------------------------------------------------------
// PURPOSE: Verify messegeCodding function - checking error handling
//  PREREQUISITES:
//  a) None
//
//  TRIGGER:
//  1) calling function with invalid codingLevel
//
//  VERIFICATION:
//  1) function returns NULL pointer
//---------------------------------------------------------------
void messegeCoddingTest3()
{
  printf("%s:%d\n", __func__, __LINE__);

  unsigned char *messageMock;
  messageMock = "~~";

  uint8_t codingLevelMock = 1;
  unsigned char *ret = messegeCodding(messageMock, codingLevelMock);

  assert(ret != NULL);

  printf("Test %s passed\n\n", __func__);
}

void uintIntoStringTest1()
{
  printf("%s:%d\n", __func__, __LINE__);

  unsigned char *messageMock;
  messageMock = "~~";

  uint8_t ldMock = 3;
  uint64_t outputMock = 5;
  uint16_t length = strlen(messageMock);
  unsigned char *coddedMessageMock = calloc(length, ldMock + 1);

  unsigned char *ret = uintIntoString(coddedMessageMock, ldMock, outputMock);

  assert(ret != NULL);
  free(coddedMessageMock);

  printf("Test %s passed\n\n", __func__);
}

void nBytesOfMessageTest1()
{
  printf("%s:%d\n", __func__, __LINE__);

  unsigned char *messageMock;
  messageMock = "~~";

  uint8_t bytesNeededMock = 3;
  uint64_t startIndexMock = 0;
  uint16_t length = strlen(messageMock);

  unsigned char *ret = nBytesOfMessage(messageMock, bytesNeededMock, startIndexMock, length);

  assert(ret != NULL);

  printf("Test %s passed\n\n", __func__);

}

void messegeDecoddingTest1()
{
  printf("%s:%d\n", __func__, __LINE__);

  unsigned char *messageMock = NULL;
  uint8_t codingLevelMock = 1;
  unsigned char *ret = messegeDecodding(messageMock, codingLevelMock);

  assert(ret == NULL);

  printf("Test %s passed\n\n", __func__);

}

void messegeDecoddingTest2()
{
  printf("%s:%d\n", __func__, __LINE__);

  unsigned char *messageMock;
  messageMock = "~~";

  uint8_t codingLevelMock = 1;
  unsigned char *ret = messegeDecodding(messageMock, codingLevelMock);

  assert(ret != NULL);

  printf("Test %s passed\n\n", __func__);

}

void messegeDecoddingTest3()
{
  printf("%s:%d\n", __func__, __LINE__);

  unsigned char *messageMock = NULL;
  uint8_t codingLevelMock = 1;
  unsigned char *ret = messegeDecodding(messageMock, codingLevelMock);

  assert(ret == NULL);

  printf("Test %s passed\n\n", __func__);

}

void getNumOfBytesNeededForEncodingTest1()
{
  printf("%s:%d\n", __func__, __LINE__);

  uint8_t codingLevelMock = 1;
  uint8_t ret = getNumOfBytesNeededForEncoding(codingLevelMock);

  assert(ret == 2);

  codingLevelMock = 2;
  ret = getNumOfBytesNeededForEncoding(codingLevelMock);

  assert(ret == 3);

  codingLevelMock = 3;
  ret = getNumOfBytesNeededForEncoding(codingLevelMock);

  assert(ret == 5);

  codingLevelMock = 18;
  ret = getNumOfBytesNeededForEncoding(codingLevelMock);

  assert(ret == 0);

  printf("Test %s passed\n\n", __func__);
}

void getNumOfBytesNeededForDecodingTest1()
{
  printf("%s:%d\n", __func__, __LINE__);

  uint8_t codingLevelMock = 1;
  uint8_t ret = getNumOfBytesNeededForDecoding(codingLevelMock);

  assert(ret == 2);

  codingLevelMock = 2;
  ret = getNumOfBytesNeededForDecoding(codingLevelMock);

  assert(ret == 4);

  codingLevelMock = 3;
  ret = getNumOfBytesNeededForDecoding(codingLevelMock);

  assert(ret == 8);

  codingLevelMock = 18;
  ret = getNumOfBytesNeededForDecoding(codingLevelMock);

  assert(ret == 0);

  printf("Test %s passed\n\n", __func__);
}

void codingTests()
{
  messegeCoddingTest1();
  messegeCoddingTest2();
  messegeCoddingTest3();

  uintIntoStringTest1();

  nBytesOfMessageTest1();

  messegeDecoddingTest1();
  messegeDecoddingTest2();
  messegeDecoddingTest3();

  getNumOfBytesNeededForEncodingTest1();

  getNumOfBytesNeededForDecodingTest1();

}
