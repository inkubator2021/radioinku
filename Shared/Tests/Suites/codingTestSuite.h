#ifndef __CODING_FUNCTIONS_TEST
#define __CODING_FUNCTIONS_TEST

#include "coding.h"
#include <assert.h>

void messegeCoddingTest1();
void messegeCoddingTest2();
void messegeCoddingTest3();

void uintIntoStringTest1();

void nBytesOfMessageTest1();

void messegeDecoddingTest1();
void messegeDecoddingTest2();
void messegeDecoddingTest3();

void getNumOfBytesNeededForEncodingTest1();

void getNumOfBytesNeededForDecodingTest1();

void codingTests();

#endif
