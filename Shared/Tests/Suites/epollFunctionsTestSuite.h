#ifndef __EPOLL_FUNCTIONS_TESTS
#define __EPOLL_FUNCTIONS_TESTS

#include <assert.h>
#include "epollFunctions.h"

void test_1_set_socket_to_non_block(void);

void test_2_set_socket_to_non_block(void);

void test_1_epoll_create(void);

void test_2_epoll_create(void);

void test_1_epoll_control(void);

void test_2_epoll_control(void);

void test_1_send_frame(void);

void test_2_send_frame(void);

void test_1_recv_frame(void);

void test_2_recv_frame(void);

void test_3_recv_frame(void);

void test_4_recv_frame(void);

void epollFunctionsTests(void);

#endif
