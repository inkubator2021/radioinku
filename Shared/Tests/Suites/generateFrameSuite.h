#ifndef generateFrame_SUITE
#define generateFrame_SUITE

void runTestsGenerateFrame(void);
void test_generateFrame(void);
void test_getRandInt(void);
void test_createHeader(void);
void test_createData(void);
void test_createHSinfo(void);
void test_createSubheaderA(void);
void test_createSubheaderB(void);
void test_createControlElementsUnlogged(void);
void test_createControlElementsLogged(void);

#endif