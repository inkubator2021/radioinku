#include "cipher2Suite.h"

//---------------------------------------------------------------
// PURPOSE:Verify ciphering function's output's length
//
//  PREREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) calling function
//  
//  VERIFICATION:
//  1) Function returns output which should be 3x longer than input
//---------------------------------------------------------------

void testOutputLength()
{
    char *input = "test of message length";
    char *output = cipher2(input);
    int inputLength = strlen(input);
    int outputLength = strlen(output);
    assert((strlen(input) * 3) == strlen(output));
  
}

//---------------------------------------------------------------
// PURPOSE:Verify ciphering function's output's
//
//  PREREQUISITES:
//  a) None
//  
//  TRIGGER:
//  1) calling function
//  
//  VERIFICATION:
//  1) Function returns ciphered output
//---------------------------------------------------------------

void testCipher2()
{
    char *input = "Jakub";
    char *output = cipher2(input);
    assert(strcmp(output, "243341411431342") == 0);
}


void runCipher2Tests()
{
    testOutputLength();
    testCipher2();
}


