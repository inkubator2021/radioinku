#ifndef LOGGER_SUITE
#define LOGGER_SUITE

void runTestsLogger(void);
void test_getTime(void);
void test_createDirectory(void);
void test_concatMessageString(void);
void test_getFileName(void);
void test_writeToFile(void);
void test_getTimeStamp(void);
void test_getID(void);
void test_loggerInit(void);
void test_logger(void);
void test_getSide(void);
void test_getLoggerInstance(void);
#endif