#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>
#include <string.h>

#include "string_to_frame.h"

//data logged frame subheader B
//---------------------------------------------------------------
// PURPOSE:Verify string_to_frame function for frame type dataFrame
// for logged user with subheader B
//
//  PREQUISITES:
//  a) Contruct char[] with proper data
//
//  TRIGGER:
//  1) running function
//
//  VERIFICATION:
//  1) Function return frame with all fields filled with expected data
//---------------------------------------------------------------

void test_string_to_frame_1()
{
    printf("%s:%d\n", __func__, __LINE__);
    char frameX[12] = {0b01110100, 0b01101100, 0b00000000, 0b10001110, 0b00000000, 0b000101000, 'A', 'B', '0', '#', 'm', '0'};
    frame res = string_to_frame(frameX);
    // Header
    assert(res.dataFrame.frameHeader.TI == 0b01110);
    assert(res.dataFrame.frameHeader.N == 2);
    assert(res.dataFrame.frameHeader.R == 0);
    // Control elements (logged)
    assert(res.dataFrame.frameControlElements.frameControlElementsLogged.UID == 3);
    assert(res.dataFrame.frameControlElements.frameControlElementsLogged.C == 1);
    assert(res.dataFrame.frameControlElements.frameControlElementsLogged.CC == 2);
    assert(res.dataFrame.frameControlElements.frameControlElementsLogged.R == 0);
    assert(res.dataFrame.frameControlElements.frameControlElementsLogged.RESERVED == 0);
    // Subheader B
    assert(res.dataFrame.frameSubheader.frameSubheaderB.F == 1);
    assert(res.dataFrame.frameSubheader.frameSubheaderB.Num = 0b000111);
    assert(res.dataFrame.frameSubheader.frameSubheaderB.L == 0);
    assert(res.dataFrame.frameSubheader.frameSubheaderB.LENGTH == 0b00000000000101000);
    // Data and padding
    // assert(strncmp(res.dataFrame.frameData.data, "AB0#m", strlen(res.dataFrame.frameData.data)) == 0);
    assert(strncmp(res.dataFrame.frameData.padding, "0", strlen(res.dataFrame.frameData.padding)) == 0);
    free(res.dataFrame.frameData.data);
    free(res.dataFrame.frameData.padding);
    printf("%s done\n", __func__);
}

//data unlogged frame subheader A
//---------------------------------------------------------------
// PURPOSE:Verify string_to_frame function for frame type dataFrame
// for unlogged user with subheader A
//
//  PREQUISITES:
//  a) Contruct char[] with proper data
//
//  TRIGGER:
//  1) running function
//
//  VERIFICATION:
//  1) Function return frame with all fields filled with expected data
//---------------------------------------------------------------
void test_string_to_frame_2()
{
    printf("%s:%d\n", __func__, __LINE__);
    char frameX[12] = {0b01010000, 0b00001101, 0b01001100, 0b00000000, 0b00001110, 0b00101000, 'A', 'B', '0', '#', 'm', '0'};
    frame res = string_to_frame(frameX);
    // Header
    assert(res.dataFrame.frameHeader.TI == 0b01010);
    assert(res.dataFrame.frameHeader.N == 0);
    assert(res.dataFrame.frameHeader.R == 0);
    // Control elements (logged)
    assert(res.dataFrame.frameControlElements.frameControlElementsUnlogged.UID == 0);
    assert(res.dataFrame.frameControlElements.frameControlElementsUnlogged.C == 1);
    assert(res.dataFrame.frameControlElements.frameControlElementsUnlogged.CC == 2);
    assert(res.dataFrame.frameControlElements.frameControlElementsUnlogged.R == 1);
    assert(res.dataFrame.frameControlElements.frameControlElementsUnlogged.RESERVED == 0);
    // Subheader B
    assert(res.dataFrame.frameSubheader.frameSubheaderA.F == 0);
    assert(res.dataFrame.frameSubheader.frameSubheaderA.Num = 0b000111);
    assert(res.dataFrame.frameSubheader.frameSubheaderA.L == 0);
    assert(res.dataFrame.frameSubheader.frameSubheaderA.LENGTH == 0b00101000);
    // Data and padding
    assert(strncmp(res.dataFrame.frameData.padding, "0", strlen(res.dataFrame.frameData.padding)) == 0);
    assert(strncmp(res.dataFrame.frameData.data, "AB0#m", strlen(res.dataFrame.frameData.data)) == 0);
    free(res.dataFrame.frameData.data);
    free(res.dataFrame.frameData.padding);
    printf("%s done\n", __func__);
}

//handshake
//---------------------------------------------------------------
// PURPOSE:Verify string_to_frame function for handshake frame
//
//  PREQUISITES:
//  a) Contruct char[] with proper data
//
//  TRIGGER:
//  1) running function
//
//  VERIFICATION:
//  1) Function return frame with all fields filled with expected data
//---------------------------------------------------------------
void test_string_to_frame_3()
{
    printf("%s:%d\n", __func__, __LINE__);
    char hsFrame[3] = {0b00001100, 0b00110010, 0b10101010};
    frame res = string_to_frame(hsFrame);

    assert(res.handshakeFrame.frameHeader.TI == 1);
    assert(res.handshakeFrame.frameHeader.N == 2);
    assert(res.handshakeFrame.frameHeader.R == 0);

    assert(res.handshakeFrame.frameHSinfo.UID == 0b00110010);
    assert(res.handshakeFrame.frameHSinfo.UID == 0b00110010);
    assert(res.handshakeFrame.frameHSinfo.NUM == 0b101010);
    assert(res.handshakeFrame.frameHSinfo.A == 1);
    assert(res.handshakeFrame.frameHSinfo.R == 0);

    printf("%s done\n", __func__);
}

void run_string_to_frame_tests()
{
    test_string_to_frame_1();
    test_string_to_frame_2();
    test_string_to_frame_3();
}


