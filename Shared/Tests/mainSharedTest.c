#include "logger.h"
#include "loggerSuite.h"
#include "generateFrame.h"
#include "tests_string_to_frame.h"
#include "generateFrameSuite.h"
#include "dataFrameSizeTestSuite.h"
#include "epollFunctionsTestSuite.h"
#include "frameSerializationTestSuite.h"
#include "frameInterpretationSuite.h"
#include "cipher2Suite.h"
#include "codingTestSuite.h"
#include "decipher2Suite.h"
#include "ceasarCipherTestSuite.h"

int main (void){
    //runTestsLogger();

    runTestsGenerateFrame();

    run_string_to_frame_tests();

    frame_serialization_tests();

    epollFunctionsTests();

    calculateDataFrameSizeTests();

    runFrameInterpretationTests();

    codingTests();
    
    runCipher2Tests();

    runDecipher2Tests();

    runCeasarCipherTests();
    
}
