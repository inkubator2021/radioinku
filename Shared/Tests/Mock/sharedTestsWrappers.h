#ifndef __TEST_SHARED
#define __TEST_SHARED

#include "epollFunctions.h"

bool mock_epoll_ctl = false;
bool mock_recv = false;

int __wrap_close(int sockfd)
{
  return sockfd;
}

int __real_epoll_ctl(int epfd, int op, int fd, struct epoll_event *event);
int __wrap_epoll_ctl(int epfd, int op, int fd, struct epoll_event *event)
{
  int ret_val = -1;
  if (mock_epoll_ctl == true)
  {
    return ret_val;
  }
  else
  {
    return __real_epoll_ctl(epfd, op, fd, event);
  }
}

int __real_recv(int sockfd, void *buff, size_t len, int flags);
int __wrap_recv(int sockfd, void *buff, size_t len, int flags)
{
  int ret_val = -1;
  if (mock_recv == true)
  {
    if (sockfd == 1)
    {
        return ret_val = 0;
    }
    else if (sockfd == 2)
    {
      return ret_val = 1;
    }
    else if (!(buff == NULL) != !(sockfd < 0))
    {
      errno = EAGAIN;
      return ret_val;
    }
  }
  else
  {
    return __real_recv(sockfd, buff, len, flags);
  }
}

#endif
