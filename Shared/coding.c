#include "coding.h"
#include <math.h>


unsigned char* messegeCodding(unsigned char *message, uint8_t codingLevel)
{
  double ld = 0;
  uint8_t bytesOfMsgNeededForEncodingOneByte = 0;

  if (message == NULL)
  {
    printf("NULL pointer error\n");
    return NULL;
  }

  bytesOfMsgNeededForEncodingOneByte = getNumOfBytesNeededForEncoding(codingLevel);
  if (bytesOfMsgNeededForEncodingOneByte == 0)
  {
    return NULL;
  }

  uint16_t length = strlen(message); //in bytes

  ld = pow(2, (double)codingLevel) - 1;

  unsigned char *coddedMessage = calloc(length, ld + 1);
  unsigned char *ptr;

  ptr = coddedMessage;

  for (uint8_t byteIndex = 0; byteIndex < length; byteIndex++)
  {
    uint64_t originalMessage = 0;

    originalMessage = nBytesOfMessage(message, bytesOfMsgNeededForEncodingOneByte, byteIndex, length);

    uint64_t output = 0;
    uint8_t readBit = (CHAR_BIT * bytesOfMsgNeededForEncodingOneByte);
    uint8_t writeBit = (CHAR_BIT * (ld + 1));

    //loop for coding 1 original byte into LD+1 bytes
    for (uint8_t i = 0; i < CHAR_BIT; ++i)
    {
      readBit--;
      //loop for coding 1 original bit into LD+1 output bits
      for (uint8_t j = 0; j <= ld; ++j)
      {
        writeBit--;
        output |= ((originalMessage >> readBit) & 1) << writeBit;
        readBit -= codingLevel + 1; // readBit = readBit - (n+1);
      }
      readBit = ((CHAR_BIT * bytesOfMsgNeededForEncodingOneByte) - i - 1);
    }
    //fills in output int ptr and moves ptr for ld + 1 bytes ahead
    ptr = uintIntoString(ptr, ld, output);
  }

  return coddedMessage;
}

//change ld+1 bytes of coded message from uint64 into char*
unsigned char* uintIntoString(unsigned char *coddedMessage, uint8_t ld, uint64_t output)
{
  for (uint8_t i = 0; i <= ld; i++)
  {
      coddedMessage[i] = output >> ((ld - i) * CHAR_BIT);
  }

  return coddedMessage + ld + 1;
}

//takes nedded bytes of original data starting from startIndex and change it into uint64
uint64_t nBytesOfMessage(unsigned char *message, uint8_t bytesNeeded, uint8_t startIndex, uint8_t stopIndex)
{
  uint64_t messageForCoding = 0;
  uint8_t cycleIndex = 0;

  for (uint8_t i = 0; i < bytesNeeded; i++)
  {
    //case when we have to encode last bytes from message
    if ((startIndex + i) >= stopIndex)
    {
      messageForCoding |= (uint64_t)(message[cycleIndex]) << ((bytesNeeded - i - 1) * CHAR_BIT);
      cycleIndex++;
      if (cycleIndex == stopIndex)
      {
        cycleIndex = 0;
      }
    }
    else
    {
      messageForCoding |= (uint64_t)(message[i + startIndex]) << ((bytesNeeded - i - 1) * CHAR_BIT);
    }
    printf("nBytesOfMessage: %lx \n", messageForCoding);
  }

  return messageForCoding;
}


unsigned char* messegeDecodding(unsigned char *message, uint8_t codingLevel)
{
  double ld = 0;
  uint8_t bytesOfMsgNeededForDecodingOneByte = 0;

  if (message == NULL)
  {
    printf("NULL pointer error\n");
    return NULL;
  }

  bytesOfMsgNeededForDecodingOneByte = getNumOfBytesNeededForDecoding(codingLevel);
  if (bytesOfMsgNeededForDecodingOneByte == 0)
  {
    return NULL;
  }

  unsigned char *ptr;
  ptr = message;
  uint16_t length = strlen(message);

  ld = pow(2, (double) codingLevel) - 1;

  uint16_t decoddedLength = length / (ld + 1);

  unsigned char *decoddedMessage = calloc(sizeof(char), decoddedLength);

  for (uint8_t byteIndex = 0; byteIndex < decoddedLength; byteIndex++)
  {
    uint64_t encodedMessage = 0;

    encodedMessage = nBytesOfMessage(ptr, bytesOfMsgNeededForDecodingOneByte, byteIndex, length);

    uint64_t output = 0;
    uint8_t readBit = (CHAR_BIT * bytesOfMsgNeededForDecodingOneByte) - 1;

    //loop for decoding 1 original byte from LD+1 encoded bytes
    for (uint8_t i = 0; i < CHAR_BIT; ++i)
    {
      decoddedMessage[byteIndex] |= ((encodedMessage >> readBit) & 1) << (CHAR_BIT - i - 1);
      readBit -= ld + 1;
    }
    ptr += bytesOfMsgNeededForDecodingOneByte - 1;
  }

  return decoddedMessage;
}


uint8_t getNumOfBytesNeededForEncoding(uint8_t codingLevel)
{
  uint8_t bytesOfMsgNeeded = 0;

  switch (codingLevel)
  {
    case 1:
      bytesOfMsgNeeded = 2;
      break;
    case 2:
      bytesOfMsgNeeded = 3;
      break;
    case 3:
      bytesOfMsgNeeded = 5;
      break;
    default:
      printf("Invalid coding level");
      return 0;
  }
  return bytesOfMsgNeeded;
}

uint8_t getNumOfBytesNeededForDecoding(uint8_t codingLevel)
{
  uint8_t bytesOfMsgNeeded = 0;

  switch (codingLevel)
  {
    case 1:
      bytesOfMsgNeeded = 2;
      break;
    case 2:
      bytesOfMsgNeeded = 4;
      break;
    case 3:
      bytesOfMsgNeeded = 8;
      break;
    default:
      printf("Invalid coding level");
      return 0;
  }
  return bytesOfMsgNeeded;
}


