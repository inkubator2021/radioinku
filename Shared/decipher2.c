#include "decipher2.h"
#define FirstCharInAsciiPosition 32
#define OutputLengthDivider 3
#define AsciiNumbersStartingPosition 48
#define IndexingDifference 1

char *decipher2(char *input)
{
    char cipheringTable [4][5][5] = {
        {{' ', '!', '"', '#', '$'},
        {'%', '&', '\'', '(', ')'},
        {'*', '+', ',', '-', '.'},
        {'/', '0', '1', '2', '3'},
        {'4', '5', '6', '7', '8'}},

        {{'9', ':', ';', '<', '='},
        {'>', '?', '@', 'A', 'B'},
        {'C', 'D', 'E', 'F', 'G'},
        {'H', 'I', 'J', 'K', 'L'},
        {'M', 'N', 'O', 'P', 'Q'}},

        {{'R', 'S', 'T', 'U', 'V'},
        {'W', 'X', 'Y', 'Z', '['},
        {'\\', ']', '^', '_', '`'},
        {'a', 'b', 'c', 'd', 'e'},
        {'f', 'g', 'h', 'i', 'j'}},

        {{'k', 'l', 'm', 'n', 'o'},
        {'p', 'q', 'r', 's', 't'},
        {'u', 'v', 'w', 'x', 'y'},
        {'z', '{', '|', '}', '~'},
        {'\0', '\0', '\0', '\0', '\0'}}
    };

    uint8_t length = strlen(input);
    if (length % 3 != 0 || length == 0)
        {
            return "error";
        }

    uint8_t outputLength = length/OutputLengthDivider;
    char *output = (char *)calloc(length/OutputLengthDivider, (length*sizeof(char))/OutputLengthDivider);
    uint8_t x,y,z;
    for (int i=0; i<outputLength; i++)
        {
            x = (int)input[i*OutputLengthDivider]-AsciiNumbersStartingPosition-IndexingDifference;
            y = (int)input[i*OutputLengthDivider+1]-AsciiNumbersStartingPosition-IndexingDifference;
            z = (int)input[i*OutputLengthDivider+2]-AsciiNumbersStartingPosition-IndexingDifference;
            output[i] = cipheringTable[x][y][z];
        }
        return output;
}
