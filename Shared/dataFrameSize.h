#ifndef __DATA_FRAME_SIZE
#define __DATA_FRAME_SIZE

#include "frameStructure.h"
#include "epollFunctions.h"

#define MAX_N 6

uint16_t calculate_dataFrame_size(uint16_t size_r);

#endif // !__DATA_FRAME_SIZE
