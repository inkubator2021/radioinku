#ifndef __CODING
#define __CODING

#include <stdlib.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <inttypes.h>
#include <limits.h>


#include "logger.h"

unsigned char* messegeCodding(unsigned char *message, uint8_t codingLevel);

unsigned char* messegeDecodding(unsigned char *message, uint8_t codingLevel);

unsigned char* uintIntoString(unsigned char *coddedMessage, uint8_t ld, uint64_t output);

uint64_t nBytesOfMessage(unsigned char *message, uint8_t bytesNeeded, uint8_t startIndex, uint8_t stopIndex);

uint8_t getNumOfBytesNeededForEncoding(uint8_t codingLevel);

uint8_t getNumOfBytesNeededForDecoding(uint8_t codingLevel);

#endif
