#include "frameInterpretation.h"
#define TI_Handshake 1
#define TI_Login 2
#define TI_Logout 3
#define TI_Reserved_Low 4
#define TI_Reserved_High 9
#define TI_AFRC 10
#define TI_IARC 11
#define TI_DataTransmission_Low 12
#define TI_DataTransmission_High 30
#define TI_Test 31

/*
-5: Logged user tries to login
-4: Reserved bits in header aren't set to 0
-3: Retransmission number out of range
-2: Couldn't resolve TI
-1: TI out of range
 2: Login
 3: Logout
 4: Reserved
 5: Ask for radio conditions
 6: Information about radio conditions
 7: Data transmission
 8: Test
 9: Handshake procedure
10: Correct handshake confirmation
11: 1st retransmission
12: 2nd retransmission
13: 3rd retransmission resulting in checking radio conditions
*/


int frameInterpretation(frame frame)
{
    if (frame.dataFrame.frameHeader.R != 0)
        return startReserved;

    switch (frame.dataFrame.frameHeader.TI)
    {
        case TI_Handshake:               
            return handshakeFrameInterpretation(frame);

        case TI_Login:              
            if (frame.dataFrame.frameControlElements.frameControlElementsLogged.UID != 0)
                return loginError;
            else
                return startLogingIn;
            
        case TI_Logout:               
            return startLogingOut;
            
        case TI_Reserved_Low ... TI_Reserved_High:
            return startReserved;

        case TI_AFRC:               
            return startAFRC;
            
        case TI_IARC:               
            return startIARC;

        case TI_DataTransmission_Low ... TI_DataTransmission_High:   
            return startDataTransmission;

        case TI_Test:               
            return startTest;

        default:
            return typeIndicatorOutOfRange;     
    }
    return typeIndicatorError;
}


int handshakeFrameInterpretation(frame frame)
{
    if (frame.handshakeFrame.frameHSinfo.A)
        return correctHandshakeConfirmation;
    else
    {
        switch (frame.handshakeFrame.frameHeader.N) //Nr retransmisji
        {
            case 0: 
                //Zmiana szyfru kodujacego
                //Inkrementacja N w headerze
                //Retransmisja 1
                return start1stRetransmission;

            case 1: 
                //Zwiekszenie poziomu kodowania o 1
                //Retransmisja 2
                return start2ndRetransmission;

            case 2: 
                //Ustalenie warunkow radiowych
                return start3rdRetransmission;

            default:
                return retransmissionNumberError;
        }
    }
}


